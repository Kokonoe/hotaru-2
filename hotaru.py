# ! /usr/bin/env python3
# -*- coding: utf-8 -*-

# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
#
# Hotaru is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Hotaru is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Hotaru.  If not, see <https://www.gnu.org/licenses/>.
"""Hotaru is an open-source and modular Discord moderation bot.

She features administrative and moderation commands, experience tracking and
levels, economy features, and help messages.

Check the README.md for detailed installation, deployment, and usage."""

import asyncio
import logging

from interactions import Client
from interactions.ext import wait_for

from app.conf import extensions
from app.log import configure_logging
from app.settings import service
from helpers import startup

if __name__ == "__main__":
    configure_logging("hotaru")
    logger = logging.getLogger("hotaru")

    loop = asyncio.get_event_loop()
    presence = loop.run_until_complete(
        service.get_default("settings", "presence"))

    if presence:
        bot: Client = startup.instantiate_bot(*presence, disable_sync=False)
    else:
        bot: Client = startup.instantiate_bot(disable_sync=False)
    for extension in extensions:
        bot.load(extension)

    wait_for.setup(bot)
    logger.log(logging.INFO, "Starting client...")
    bot.start()
