# Hotaru

## Description

Hotaru is having none of your nonsense.

## Features

Hotaru has lots of features.

## Getting Started

1. Install requirements
2. Install mongo
2.5: Something about [replica sets](https://stackoverflow.com/questions/51461952/mongodb-v4-0-transaction-mongoerror-transaction-numbers-are-only-allowed-on-a)
3. Adjust config
4. Insert token
5. Let her rip!

## Usage

/help

## Contact

kokonoemercury@protonmail.com
