# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""A demonstration cog for ActionRows, Buttons, and SelectMenus."""

from interactions import (
    ActionRow,
    Button,
    ButtonStyle,
    Client,
    CommandContext as sctx,
    ComponentContext as cctx,
    Emoji,
    Extension,
    extension_command as command,
    extension_component as component,
    SelectMenu,
    SelectOption,
)

from app.conf import scope
from helpers.basics import quip

buttons = {
    "red":
    Button(style=ButtonStyle.DANGER, label="A Red Button", custom_id="red"),
    "green":
    Button(style=ButtonStyle.SUCCESS,
           label="A Green Button",
           custom_id="green"),
    "blue":
    Button(style=ButtonStyle.PRIMARY, label="A Blue Button", custom_id="blue"),
}

menu = {
    "science":
    SelectMenu(
        options=[
            SelectOption(label="Lab Coat", value="coat",
                         emoji=Emoji(name="🥼")),
            SelectOption(label="Test Tube",
                         value="tube",
                         emoji=Emoji(name="🧪")),
            SelectOption(label="Petri Dish",
                         value="dish",
                         emoji=Emoji(name="🧫")),
        ],
        placeholder="Select your science.",
        min_values=1,
        max_values=3,
        custom_id="science",
    )
}


class Component(Extension):

    def __init__(self, bot: Client):
        self.bot = bot

    @command(name="buttons", scope=scope)
    async def make_buttons(self, ctx: sctx):
        """Want some buttons? We've got buttons."""
        await quip(ctx, components=ActionRow.new(*buttons.values()))

    @component(buttons["red"])
    async def button_red(self, ctx: cctx):
        await quip(ctx, components=None)

    @component(buttons["green"])
    async def button_green(self, ctx: cctx):
        await quip(ctx, components=None)

    @component(buttons["blue"])
    async def button_blue(self, ctx: cctx):
        await quip(ctx, components=None)

    @command(name="selections", scope=scope)
    async def make_selections(self, ctx: sctx):
        """Selections? We have selections."""
        await quip(ctx, components=ActionRow.new(*menu.values()))

    @component(menu["science"])
    async def selects_listener(self, ctx: cctx, selection: list[str]):
        await quip(ctx, components=None, options=" and a ".join(selection))


def setup(bot: Client):
    Component(bot)
