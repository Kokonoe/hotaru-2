# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury

from interactions import (
    Choice,
    CommandContext as sctx,
    Emoji,
    Extension,
    extension_command as command,
    Guild,
    option,
)

from app.conf import scope
from helpers.basics import Client, say


class Emojis(Extension):

    def __init__(self, bot: Client):
        self.bot = bot

    @command(scope=scope)
    async def emoji(self, ctx: sctx) -> None:
        pass

    subcommand = emoji.subcommand

    @subcommand()
    @option(description="The emote to post.", required=True, autocomplete=True)
    async def post(self, ctx: sctx, emoji: str) -> None:
        """Post an emoji from the guild, even animated!

        Args:
            emoji: The emoji's name.
        """
        assert ctx.guild
        guild: Guild = ctx.guild

        if emoji == "":
            return
        if not guild.emojis:
            return

        emotes: list[Emoji] = guild.emojis
        for emote in emotes:
            if emote.name == emoji:
                await ctx.send(
                    f"<{'a' if emote.animated else ''}:{emote.name}:{emote.id}>"
                )
                return

    @subcommand()
    @option(description="The emote to copy.", required=True, autocomplete=True)
    async def link(self, ctx: sctx, emoji: str) -> None:
        """Get a link for a guild emoji that you can post anywhere!

        Args:
            emoji: The emoji's name.
        """
        assert ctx.guild
        guild: Guild = ctx.guild

        if emoji == "":
            return
        if not guild.emojis:
            return

        emotes: list[Emoji] = guild.emojis
        for emote in emotes:
            if emote.name == emoji:
                url = "https://cdn.discordapp.com/emojis/"
                extension = '.gif' if emote.animated else '.webp'
                args = "?size=48&quality=lossless"
                await say(ctx, f"```{url}{emote.id}{extension}{args}```")
                return

    @post.autocomplete("emoji")
    async def post_autocomplete(self, ctx: sctx, input: str = "") -> None:
        await self.emoji_autocomplete(ctx, input)

    @link.autocomplete("emoji")
    async def link_autocomplete(self, ctx: sctx, input: str = "") -> None:
        await self.emoji_autocomplete(ctx, input)

    async def emoji_autocomplete(self, ctx: sctx, input: str = "") -> None:
        assert ctx.guild
        guild: Guild = ctx.guild
        no_matches: Choice = Choice(name="No matches found.", value="")
        if guild.emojis:
            emote_names: list[str] = sorted([
                emoji.name
                for emoji in guild.emojis
                if emoji.name is not None and input in emoji.name
            ])
            await ctx.populate([
                Choice(name=emote_name, value=emote_name)
                for emote_name in emote_names[:25]
            ] or [no_matches])
        else:
            await ctx.populate(no_matches)


def setup(bot: Client):
    Emojis(bot)
