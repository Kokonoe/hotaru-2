# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""Basic helper functions to simplify message handling."""

import types
from typing import TypeAlias

from interactions import (
    ActionRow,
    Button,
    ButtonStyle as style,
    Channel,
    Client as InteractionsClient,
    CommandContext as sctx,
    ComponentContext as cctx,
    Embed as InteractionsEmbed,
    Guild,
    HTTPClient,
    Message,
    OptionType as opt,
    SelectMenu,
    Snowflake,
)
from interactions.ext.wait_for import wait_for, wait_for_component

from app import conf
from helpers.emotes import ReactionEmotes

Components: TypeAlias = ActionRow | Button | SelectMenu | list[
    ActionRow] | list[Button] | list[SelectMenu] | None


class Client(InteractionsClient):
    """Subclass of Interactions Client with wait_for commands."""

    def __init__(self, token: str, **kwargs) -> None:
        super().__init__(token, **kwargs)
        self.wait_for = types.MethodType(wait_for, self)
        self.wait_for_component = types.MethodType(wait_for_component, self)


class Embed(InteractionsEmbed):
    """Interactions.Embed initialized with Hotaru's configured color.

    Args:
        title (str): Title of embed
        type (str): Embed type, relevant by CDN file connected.
            This is only important to rendering.
        description (str): Embed description
        url (str): URL of embed
        timestamp (DateTime): Timestamp of embed content
        color (int): Color code of embed
        footer (EmbedFooter): Footer information
        image (EmbedImageStruct): Image information
        thumbnail (EmbedImageStruct): Thumbnail information
        video (EmbedImageStruct): Video information
        provider (EmbedProvider): Provider information
        author (EmbedAuthor): Author information
        fields (list[EmbedField]): A list of fields denoting field information

    Returns:
        Embed
    """

    def __init__(self, **kwargs) -> None:
        super().__init__(color=conf.config["color"], **kwargs)


async def playing(bot: Client, type: str, phrase: str):
    """Change Hotaru's presence."""
    # TODO: Implement change presence.


async def say(ctx: cctx | sctx | Channel,
              description: str,
              components: Components = None,
              hidden: bool = False) -> Message:
    """Post a message in an Embed.

    Args:
        ctx: The Context or Channel to send the message in.
        description: The message to say.
        components: A list of message components.
        hidden: If True, message is ephemeral.

    Returns:
        Message: The sent message object.
    """
    embed: Embed = Embed(description=description)
    if isinstance(ctx, (cctx, sctx)):
        return await ctx.send(embeds=embed,
                              components=components,
                              ephemeral=hidden)
    elif isinstance(ctx, Channel):
        return await ctx.send(embeds=embed, components=components)


async def edit(ctx: cctx | sctx | Message, description: str,
               components: Components) -> Message:
    """Edit a the message from a Context or Message object.

    Args:
        ctx: The Context or Channel to send the message in.
        description: The message to say.
        components: A list of message components.
            If MISSING, re-use components from original Message.

    Returns:
        Message: The sent message object.
    """
    embed: Embed = Embed(description=description)
    return await ctx.edit(embeds=embed, components=components)


async def get_quip(ctx: cctx | sctx | None = None,
                   line: str = "default",
                   *,
                   command: str | None = None,
                   group: str | None = None,
                   subcommand: str | None = None,
                   **format) -> str | None:
    """Get a quip from the quips list.

    Useful for when you don't want to immediately send / edit a message like
    quip() will. If a Context is provided, command, group (subcommand_group),
    and subcommand will be extracted from it, otherwise they should be provided
    as kwargs.

    Args:
        ctx: The Context to derive keys from.
        line: The dictionary key to retrieve the response from.
        command: The command or command_base.
        group: The subcommand_group.
        subcommand: The subcommand.
        **format: kwargs for string format interpolation.

    Returns:
        str: A quip from the quips list.
    """
    if ctx:
        assert ctx.data.options
        if isinstance(ctx, sctx):
            command = ctx.data.name
            has_subcommand: bool = (
                ctx.data.options[0].type
                == opt.SUB_COMMAND) if ctx.data.options is not None else False
            if command is not None:
                if has_subcommand:
                    subcommand = ctx.data.options[0].name
        elif isinstance(ctx, cctx):
            if ctx.data.custom_id is not None:
                key: str = ctx.data.custom_id
                if "." in key:
                    key = key.partition(".")[0]
                command = "components"
                subcommand = key
    quip: str = ""
    if command and not subcommand:
        quip = await conf.quips[command][line]
    elif command and subcommand and not group:
        quip = await conf.quips[command][subcommand][line]
    elif command and subcommand and group:
        quip = await conf.quips[command][group][subcommand][line]
    if quip:
        return quip.format(**format)


async def quip(ctx: cctx | sctx,
               line: str = "default",
               components: Components = None,
               hidden: bool = False,
               message: Message | None = None,
               **format: str) -> Message:
    """Read a witty remark from data/quips.yml

    quips.yml contains a dictionary with Hotaru's responses to commands.
    The dictionary is structured like so:

        # Standalone commands
        command:
            default: 'The default message to respond with'
            alternate: 'An alternate message accessed via the line argument'

        # Subcommands
        base_command:
            subcommand:
                default: 'The subcommand's default message'
                alternate: 'Subcommands can have alternate lines, too!'

        # Components
        components:
            custom_id: 'Component responses are accessed by their custom_id.'

    The responses can contain expressions in curly braces for interpolation.
    The expressions' arguments are passed as kwargs.

    Example:
        command:
            default: '{user} is charming and funny!'

        await quip(ctx, user='Kokonoe')

        'Kokonoe is charming and funny!'

    Args:
        ctx: The CommandContext or ComponentContext to respond to.
        line: The dictionary key to retrieve the response from.
        components: A list of message components.
            If MISSING, re-use components from original Message.
        hidden: If True, message is ephemeral.
        message: The Message object, needed for repeat edits.
            If using this, don't call ctx.edit() during the command!
        **format: kwargs for string format interpolation.

    Returns:
        Message: The sent message object.
    """
    if isinstance(ctx, sctx):
        command: str = ctx.data.name
        # TODO: Support for subcommand groups i.e. another potential nesting layer
        has_subcommand: bool = (ctx.data.options[0].type == opt.SUB_COMMAND
                                ) if ctx.data.options is not None else False

        if command is None:
            raise TypeError("ctx.data.name returned None.")
        else:
            if has_subcommand:
                assert ctx.data.options
                subcommand: str = ctx.data.options[0].name
                quip: str = conf.quips[command][subcommand][line]
            else:
                quip: str = conf.quips[command][line]
            embed: Embed = Embed(description=quip.format(**format))
            return await ctx.send(embeds=embed,
                                  components=components,
                                  ephemeral=hidden)
    elif isinstance(ctx, cctx):
        if ctx.data.custom_id is None:
            raise TypeError("ctx.data.custom_id returned None.")
        else:
            key: str = ctx.data.custom_id
            if "." in key:
                key = key.partition(".")[0]
            quip = conf.quips["components"][key][line]
            embed: Embed = Embed(description=quip.format(**format))
            if message:
                return await message.edit(embeds=embed, components=components)
            else:
                return await ctx.edit(embeds=embed, components=components)
    else:
        raise TypeError("Argument ctx not expected type.")


async def get_command_list(bot: Client,
                           guild_id: Snowflake | int | None = None,
                           base_only: bool = False) -> list[str]:
    """List all the commands in the bot.

    If guild_id is provided, limits scope to that guild, otherwise returns all
    the commands in the bot.

    Args:
        bot: The active Client.
        guild_id: If provided, limits scope to that guild.
        base_only: If True, only return a list of base commands.

    Returns:
        list[str]: A list of dot-separated commands in the format of:
            "command"
            "command.subcommand"
            "command.subcommand_group.subcommand"
    """
    assert bot.me
    assert bot.me.id
    assert isinstance(bot._http, HTTPClient)

    if isinstance(guild_id, Snowflake):
        guild_id = int(guild_id)
    commands: list[dict] = await bot._http.get_application_commands(
        bot.me.id, guild_id)
    results: list[str] = []
    for command in commands:
        base: str = command.get("name", None)
        if base_only:
            results.append(base)
            continue
        group: str | None = None
        subcommand: str | None = None
        first_options: list[dict] = command.get("options", [])
        for first_option in first_options:
            if first_option.get("type", None) == opt.SUB_COMMAND:
                subcommand = first_option.get("name", "")
                assert subcommand
                results.append(".".join([base, subcommand]))
            elif first_option.get("type", None) == opt.SUB_COMMAND_GROUP:
                group = first_option.get("name", "")
                assert group
                second_options: list[dict] = first_option.get("options", [])
                for second_option in second_options:
                    if second_option.get("type", None) == opt.SUB_COMMAND:
                        subcommand = second_option.get("name", "")
                        assert subcommand
                        results.append(".".join([base, group, subcommand]))
        if not group and not subcommand:
            results.append(base)

    return sorted(results)


async def make_buttons(ctx: sctx | Guild | Message, id_prefix: str,
                       *buttons: str) -> dict[str, Button]:
    """A utility to make prefixed buttons with stored emoji icons.

    Args:
        ctx: An object with a guild_id, e.g. Guild, Message, or CommandContext.
        id_prefix: A shared prefix for the custom_id. Particularly useful
            when combined with interactions-enhanced's component callbacks.
        *buttons: The button names to make buttons for.

    Returns:
        dict[str, Button]: A dictionary of the created buttons.
    """
    emotes = await ReactionEmotes.create(ctx)
    buttons_dict: dict[str, Button] = {
        button: Button(style=style.SECONDARY,
                       label=button,
                       emoji=emotes.get(button),
                       custom_id=".".join([id_prefix, button]))
        for button in buttons
    }
    return buttons_dict
