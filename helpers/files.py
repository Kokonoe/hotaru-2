# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""Helper functions for file operations."""


def read_text(file_path: str) -> list[str]:
    """A simple text file loader.

    Args:
        file_path: The file location, relative to hotaru's root.

    Returns:
        list[str]: A list of the file's lines.
    """
    items: list[str] = []
    try:
        with open(file_path, "r") as f:
            for i in f:
                items.append(i.strip())
    except FileNotFoundError:
        print(f"{file_path} not found.")
    return items
