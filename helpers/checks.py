# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""Permissions checks for commands.

Checks can be integrated with the commands framework by placing them under the
@command or @component decorator:

    @command(...)
    @check(*args, **kwargs)
    async def command(ctx):
        await do_something()
"""

from asyncio import iscoroutinefunction
from functools import wraps
from typing import Awaitable, Callable, cast, Concatenate, ParamSpec, TypeVar

from interactions import (
    CommandContext as sctx,
    ComponentContext as cctx,
    Extension,
    Guild,
    Member,
    Permissions as gperm,
)

from app.conf import owners
from helpers.basics import say
from helpers.permissions import CommandPermissions as cperm

T = TypeVar("T")
P = ParamSpec("P")


def check(predicate: Callable[..., bool | Awaitable[bool]]) -> Callable:
    """The check decorator wraps commands to add checks to their execution.

    You can use this in one of two ways. The first option would be to decorate
    a command with check() directly, supplying your own predicate. A predicate
    is a function that takes in exactly two arguments: self and one of
    either/or CommandContext and ComponentContext, and returns a bool.

        def predicate(self, ctx: CommandContext | ComponentContext) --> bool:
            return True if ctx.author.id in list_of_cool_people else False

        @check(predicate)
        async def command_for_cool_people_only(ctx: Command):
            await ctx.send(
                f"{ctx.member.nick or ctx.author.name} is really cool!")

    The second option would be to create your own decorators that define a
    predicate and return check(predicate). This has the added benefit of
    allowing you to supply arguments to the decorator for additional
    flexibility.

        def name_only(name: str):

            def predicate(ctx: CommandContext):
                return True if (
                    ctx.member.nick == name or ctx.author.name == name
                ) else False

            return check(predicate)

        @name_only("kokonoe")
        async def kokonoe_only_command(ctx: CommandContext):
            await ctx.send("You look like a Kokonoe, alright.")

    As an extra convenience, the predicate you supply in the second method
    will be attached to the decorator as an attribute, so you can access it
    from outside the wrapper function. This aids in testing:

        def test_kokonoe_only_command():
            ctx = MockContext()
            predicate = name_only("kokonoe").predicate
            assert predicate(ctx) is True

    Args:
        predicate: A callable that takes a Context argument and returns a bool.
            Take note that you're feeding the function object as an argument,
            not the called function, so don't add a parenthesis to it!

    Returns:
        A decorator that can wrap a command function to add check functionality.
        The decorator has a .predicate attribute to access the predicate
        from outside the wrapper's scope.
    """

    def decorator(
        func: Callable[Concatenate[Extension, sctx | cctx, P], Awaitable[T]]
    ) -> Callable[Concatenate[Extension, sctx | cctx, P], Awaitable[T | None]]:

        @wraps(func)
        async def wrapper(self, ctx: sctx | cctx, *args: P.args,
                          **kwargs: P.kwargs) -> T | None:
            if iscoroutinefunction(predicate):
                test: bool = await cast(Awaitable[bool], predicate(self, ctx))
            else:
                test: bool = cast(bool, predicate(self, ctx))
            if test:
                return await func(self, ctx, *args, **kwargs)
            else:
                if isinstance(ctx, sctx):
                    await say(ctx,
                              f"Check for command {ctx.data.name} has failed.",
                              hidden=True)
                elif isinstance(ctx, cctx):
                    await say(
                        ctx,
                        f"Check for component {ctx.data.custom_id} has failed."
                    )

        return wrapper

    decorator.predicate = predicate
    return decorator


def is_owner(arg=None):
    """A decorator to check if the invoking user owns the bot.

    The owners' list is configured in data/config.yml. Should only be used for
    bot management and not guild management, since the bot owner shouldn't
    necessarily have authority over all the guilds the bot is in.

    The argument allows the decorator to be called with or without a
    parenthesis.

    Returns:
        A decorator that can wrap a command function to add check functionality.
        The decorator has a .predicate attribute to access the predicate
        from outside the wrapper's scope.
    """

    def predicate(self, ctx: sctx):
        assert ctx.author
        return str(ctx.author.id) in owners

    if callable(arg):
        return check(predicate)(arg)
    else:
        return check(predicate)


def has_guild_permissions(permissions: gperm = gperm(0)):
    """A decorator to check if the invoking user has guild permissions.

    Args:
        *permissions: A list of interactions.Permissions the user may have to
            allow them to invoke the command. If the user has any of the
            permissions in this list, the predicate will return True. If no
            permissions are given, the predicate will always return False.

    Returns:
        A decorator that can wrap a command function to add check functionality.
        The decorator has a .predicate attribute to access the predicate
        from outside the wrapper's scope.
    """

    async def predicate(self, ctx: sctx):
        assert ctx.author
        if hasattr(self, "settings") and self.settings.get(
                "guild_permissions", False):
            nonlocal permissions
            db_permissions = gperm(
                await self.settings.get("guild_permissions").get())
            permissions = permissions | db_permissions
        if ctx.author.permissions is None:
            return False
        return True if ctx.author.permissions & permissions != 0 else False

    if callable(permissions):
        return check(predicate)(permissions)
    else:
        return check(predicate)


def has_command_permissions(session=None):
    """A decorator to check for command permissions from the database.

    Commands can have a list of user or role ids organized into whitelists or
    blacklists. The blacklist has priority over the whitelist, so a member in
    both lists will be blocked. The user list has priority over the roles list.
    If a role is whitelisted, but a user with that role is blacklisted, they
    will be blocked.

    Args:
        session: A motor session object to enable database transaction control.

    Returns:
        A decorator that can wrap a command function to add check functionality.
        The decorator has a .predicate attribute to access the predicate
        from outside the wrapper's scope.
    """

    async def predicate(self, ctx: sctx):
        assert ctx.member

        permissions: cperm = cperm(ctx, session=session)
        await permissions.get()
        member: Member = ctx.member
        member_id: str = str(member.id)

        if member_id in permissions.perms.users_blacklist:
            return False
        if member_id in permissions.perms.users_whitelist:
            return True

        guild: Guild = await ctx.get_guild()
        assert guild.roles
        guild_roles: list[str] = [
            str(role.id) for role in sorted(
                guild.roles, key=lambda x: x.position, reverse=True)
        ]

        for role in guild_roles:
            assert member.roles
            if role in [str(member_role) for member_role in member.roles]:
                if role in permissions.perms.roles_blacklist:
                    return False
                if role in permissions.perms.roles_whitelist:
                    return True

        return False

    if callable(session):
        return check(predicate)(session)
    else:
        return check(predicate)


def has_permissions(*permissions: gperm, session=None):
    """A decorator that combines the guild and command permissions decorators.

    Fulfilling either check will allow access to the command.

    Args:
        *permissions: A list of interactions.Permissions the user may have to
            allow them to invoke the command. If the user has any of the
            permissions in this list, the predicate will return True. If no
            permissions are given, the predicate will always return False.
        session: A motor session object to enable database transaction control.

    Returns:
        A decorator that can wrap a command function to add check functionality.
        The decorator has a .predicate attribute to access the predicate
        from outside the wrapper's scope.
    """

    async def predicate(self, ctx: sctx):
        return (await has_guild_permissions(*permissions).predicate(self, ctx)
                or await has_command_permissions(session=session).predicate(
                    self, ctx))

    return check(predicate)


def allowed_channel(session=None):
    """A decorator that checks if the command can be run in the current channel.

    The channel blacklist has priority over the whitelist. A channel in either
    list has priority over the global channels_allow_all setting.

    Args:
        session: A motor session object to enable database transaction control.

    Returns:
        A decorator that can wrap a command function to add check functionality.
        The decorator has a .predicate attribute to access the predicate
        from outside the wrapper's scope.
    """

    async def predicate(self, ctx: sctx):
        if ctx.guild_id is None:
            return False
        permissions: cperm = cperm(ctx, session=session)
        await permissions.get()
        channel_id: str = str(ctx.channel_id)
        if channel_id in permissions.perms.channels_blacklist:
            return False
        if channel_id in permissions.perms.channels_whitelist:
            return True
        return permissions.perms.channels_allow_all

    if callable(session):
        return check(predicate)(session)
    else:
        return check(predicate)
