# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""Permissions objects to assist with database management."""

from __future__ import annotations

from interactions import CommandContext as sctx, ComponentContext as cctx

from app.settings import service


class PermissionOptions:
    """A helper class to handle JSON-ifying permission options."""

    def __init__(self):
        self.enabled: bool = False
        self.channels_allow_all: bool = False
        self.channels_whitelist: set[str] = set()
        self.channels_blacklist: set[str] = set()
        self.roles_whitelist: set[str] = set()
        self.roles_blacklist: set[str] = set()
        self.users_whitelist: set[str] = set()
        self.users_blacklist: set[str] = set()

    def to_dict(self) -> dict:
        """Converts the PermissionOptions object to a JSON dictionary.

        Returns:
            dict: A JSON-compatible dictionary version of This object's
                permissions.
        """
        perms: dict = {
            "enabled": self.enabled,
            "channels_allow_all": self.channels_allow_all,
            "channels_whitelist": list(self.channels_whitelist),
            "channels_blacklist": list(self.channels_blacklist),
            "roles_whitelist": list(self.roles_whitelist),
            "roles_blacklist": list(self.roles_blacklist),
            "users_whitelist": list(self.users_whitelist),
            "users_blacklist": list(self.users_blacklist),
        }
        return perms

    @staticmethod
    def from_dict(perm: dict) -> PermissionOptions:
        """Creates a new PermissionOptions object from a JSON dictionary.

        Args:
            perm: A dictionary of permission options.

        Returns:
            PermissionOptions: A PermissionOptions object representation of the
                json dictionary.
        """
        self: PermissionOptions = PermissionOptions()
        self.enabled = perm.get("enabled", False)
        self.channels_allow_all = perm.get("channels_allow_all", False)
        self.channels_whitelist = set(perm.get("channels_whitelist", []))
        self.channels_blacklist = set(perm.get("channels_blacklist", []))
        self.roles_whitelist = set(perm.get("roles_whitelist", []))
        self.roles_blacklist = set(perm.get("roles_blacklist", []))
        self.users_whitelist = set(perm.get("users_whitelist", []))
        self.users_blacklist = set(perm.get("users_blacklist", []))
        return self


class CommandPermissions:
    """A helper class to assist with managing permissions.

    Includes database management methods and default permission settings.

    Args:
        ctx: The CommandContext from the invoked slash command.
        command: The base command to read / write permissions from / to.
        perms: A pre-existing permissions object you might have for some reason.
            I can't think of a use case for this.
        session: A motor session object to enable database transaction control.
    """

    def __init__(self,
                 ctx: sctx | cctx,
                 command: str | None = None,
                 perms: PermissionOptions | None = None,
                 session=None) -> None:
        self.command: str = command or ctx.data.name
        self.guild_id: str = str(ctx.guild_id)
        self.perms: PermissionOptions = perms or PermissionOptions()
        self.is_real: bool | None = None
        self.session = session

    async def init(self) -> CommandPermissions:
        """Drop the entire permissions table from the context's guild.

        Returns:
            This CommandPermissions object for method chaining.
        """
        await service.drop(str(self.guild_id),
                           "Permissions",
                           session=self.session)
        return self

    async def get(self) -> CommandPermissions:
        """Get the command's permissions from the database.

        Returns:
            This CommandPermissions object for method chaining.
        """
        perms: dict = await service.get(str(self.guild_id),
                                        "Permissions",
                                        self.command,
                                        session=self.session)
        self.perms = PermissionOptions.from_dict(
            perms) if perms is not None else PermissionOptions()
        return self

    async def set(self) -> CommandPermissions:
        """Set the command's permissions to the database.

        Returns:
            This CommandPermissions object for method chaining.
        """
        perms: dict = self.perms.to_dict()
        await service.set(str(self.guild_id),
                          "Permissions",
                          self.command,
                          perms,
                          session=self.session)
        return self

    def clear(self) -> CommandPermissions:
        """Reset the permissions of this command.

        It's recommended to use this before setting one of the default
        permissions.

        Returns:
            This CommandPermissions object for method chaining.
        """
        self.perms = PermissionOptions()
        return self

    def permit(self) -> CommandPermissions:
        """Set permissive default permissions for this command.

        This enables the command, allows it in all channels, and adds this
        guild's @everyone role to the roles whitelist. It's recommended to run
        clear() first before this.

        Returns:
            This CommandPermissions object for method chaining.
        """
        self.perms.enabled = True
        self.perms.channels_allow_all = True
        self.perms.roles_whitelist.add(self.guild_id)
        return self

    def restrict(self) -> CommandPermissions:
        """Set restrictive default permissions for this command.

        The disables the command and disallows it from all channels (meaning
        channels must be individually added to the whitelist). It's recommended
        to run clear() first before this.

        Returns:
            This CommandPermissions object for method chaining.
        """
        self.perms.enabled = False
        self.perms.channels_allow_all = False
        return self
