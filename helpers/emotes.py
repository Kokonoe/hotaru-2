# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury

from __future__ import annotations

from interactions import (
    CommandContext as sctx,
    Guild,
    Emoji,
    Message,
)

from app.settings import service


class ReactionEmotes:

    def __init__(self):
        self.guild_id: str = ""
        self.guild_emotes: dict[str, Emoji] = {}
        self.global_emotes: dict[str, Emoji] = {}
        self.default_emotes: dict[str, Emoji] = {
            "yes": Emoji(name="🇾"),
            "no": Emoji(name="🇳"),
            "error": Emoji(name="❌"),
            "pin": Emoji(name="📌"),
            "unpin": Emoji(name="❌"),
            "prune": Emoji(name="🗑️"),
            "okay": Emoji(name="👍"),
            "done": Emoji(name="❤️"),
        }

    @classmethod
    async def create(cls, ctx: Guild | Message | sctx) -> ReactionEmotes:
        self = ReactionEmotes()
        if not isinstance(ctx, Guild):
            self.guild_id = str(ctx.guild_id)
        else:
            self.guild_id = str(ctx.id)
        self.guild_emotes = await service.get(self.guild_id, "emotes",
                                              "commands") or {}
        self.global_emotes = await service.get_default("emotes",
                                                       "commands") or {}
        return self

    def get(self, name: str) -> Emoji | None:
        return (self.guild_emotes.get(name) or self.global_emotes.get(name) or
                self.default_emotes.get(name))

    async def set_local(self, name: str, emote: Emoji) -> ReactionEmotes:
        if name in self.default_emotes:
            self.guild_emotes[name] = emote
            await service.set(self.guild_id, "emotes", "commands",
                              self.guild_emotes)
        return self

    async def reset_local(self, name: str) -> ReactionEmotes:
        if name in self.guild_emotes:
            self.guild_emotes.pop(name)
            await service.set(self.guild_id, "emotes", "commands",
                              self.guild_emotes)
        return self

    async def set_global(self, name: str, emote: Emoji) -> ReactionEmotes:
        if name in self.default_emotes:
            self.global_emotes[name] = emote
            await service.set_default("emotes", "commands", self.global_emotes)
        return self

    async def reset_global(self, name: str) -> ReactionEmotes:
        if name in self.global_emotes:
            self.global_emotes.pop(name)
            await service.set_default("emotes", "commands", self.global_emotes)
        return self
