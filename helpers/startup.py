# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""Helper functions for startup."""

import os
import logging

from interactions import (
    Client,
    ClientPresence,
    Intents,
    PresenceActivity,
    PresenceActivityType as ptype,
    StatusType,
)
import yaml


def get_extensions(optional_cogs: list[str] = [],
                   excluded_cogs: list[str] = [],
                   external_cogs: list[str] = []) -> list[str]:
    """Check cog folders and return a list of extensions to load.

    Args:
        optional_cogs: Cogs in the cogs_optional folder aren't loaded by
            default. Cogs listed here by module name will be loaded.
        excluded_cogs: Cogs in the cogs folder are loaded by default. Cogs
            listed here by module name will be excluded.
        external_cogs: Cogs that aren't in the cogs or cogs_optional folders
            can be loaded by listing them here by path.

    Returns:
        list[str]: The list of qualified extensions to load.
    """
    if optional_cogs is None:
        optional_cogs = []
    if excluded_cogs is None:
        excluded_cogs = []
    if external_cogs is None:
        external_cogs = []

    extensions_list = []

    try:
        cogs = os.listdir("cogs")
        for cog in cogs:
            if (cog.endswith(".py") and cog != "__init__.py" and
                    cog.partition(".py")[0] not in excluded_cogs):
                cog = "cogs." + cog.partition(".py")[0]
                extensions_list.append(cog)
    except FileNotFoundError:
        print("No /cogs directory detected.")

    try:
        cogs_optional = os.listdir("cogs_optional")
        for cog in cogs_optional:
            if (cog.endswith(".py") and
                    cog.partition(".py")[0] in optional_cogs):
                cog = "cogs_optional." + cog.partition(".py")[0]
                extensions_list.append(cog)
    except FileNotFoundError:
        print("No /cogs_optional directory detected.")

    external: str = ""
    try:
        for cog in external_cogs:
            external = cog
            if cog.endswith(".py"):
                extensions_list.append(cog)
    except FileNotFoundError:
        print(f"{external} not found.")

    return extensions_list


def get_config(config: str | None = None) -> dict:
    """Check for a config.yml file that has Hotaru's configuration.

    Args:
        config: If supplied, return that key from the config file instead of the
            entire dictionary.

    Returns:
        dict: A dictionary of settings.
    """

    try:
        with open("data/config.yml", "r") as file:
            configs = yaml.safe_load(file)
        if config is not None:
            return configs[config]
        else:
            return configs

    except FileNotFoundError:
        print("No config.yml present.")
        exit()


def get_quips() -> dict:
    """Load Hotaru's quips from a quips.yml file.

    Returns:
        dict: A dictionary of quips."""

    try:
        with open("data/quips.yml", "r") as file:
            quips = yaml.safe_load(file)
        return quips

    except FileNotFoundError:
        print("No quips.yml present.")
        exit()


def get_token() -> str:
    """Check for a token.yml file that has Hotaru's login token.

    Returns:
        str: Hotaru's login token."""

    try:
        with open("data/token.yml", "r") as file:
            token = yaml.safe_load(file)
        return token

    except FileNotFoundError:
        print("No token.yml present.")
        exit()


def instantiate_bot(type: int = ptype.LISTENING,
                    name: str = "the fans.",
                    disable_sync: bool = True,
                    log_level: int = logging.WARNING) -> Client:
    """Instantiates the bot.

    Automatically loads the enhanced extension, as it's a required module.
    Presence can be set during bot instantiation.

    Args:
        type: The interactions.ActivityType of Hotaru's presence.
        name: The name of Hotaru's presence.
        disable_sync: Prevents the bot from auto-syncing commands. Auto-syncing
            commands will often incur a rate-limit from Discord, so it should
            normally be disabled unless a new command is made or a command's
            options are changed. A method's behavior can be modified freely
            without the need to re-sync, so long as its name and options aren't
            changed.

    Returns:
        Client: The bot's Client instance.
    """

    presence = ClientPresence(
        activities=[PresenceActivity(name=name, type=ptype(type))],
        status=StatusType.ONLINE)

    bot = Client(intents=Intents.ALL,
                 presence=presence,
                 token=get_token(),
                 disable_sync=disable_sync,
                 logging=log_level)

    return bot
