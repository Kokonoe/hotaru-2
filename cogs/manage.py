# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""Bot management functions for the bot owner."""

import os
import subprocess
from typing import NoReturn

from interactions import (
    Choice,
    ClientPresence,
    CommandContext as sctx,
    Extension,
    PresenceActivity,
    extension_command as base,
    option,
    PresenceActivityType as ptype,
    StatusType,
)

from app.conf import scope
from app.settings import service
from helpers.basics import Client, Embed, get_command_list, quip, say
from helpers.checks import is_owner


class Manage(Extension):
    """Owner-facing commands for bot maintenance."""

    def __init__(self, bot: Client):
        self.bot = bot

    @base(scope=scope)
    async def manage(self, ctx: sctx) -> None:
        pass

    subcommand = manage.subcommand

    @subcommand()
    @option(description="The presence activity type.",
            required=True,
            choices=[
                Choice(name="game", value=ptype.GAME),
                Choice(name="streaming", value=ptype.STREAMING),
                Choice(name="listening", value=ptype.LISTENING),
                Choice(name="watching", value=ptype.WATCHING),
                Choice(name="custom", value=ptype.CUSTOM),
                Choice(name="competing", value=ptype.COMPETING)
            ])
    @option(description="The title to display.", required=True)
    @is_owner
    async def presence(self, ctx: sctx, activity: int, title: str) -> None:
        """Changes the bot's presence."""
        await self.bot.change_presence(
            ClientPresence(
                status=StatusType.ONLINE,
                activities=[PresenceActivity(name=title,
                                             type=ptype(activity))]))
        await service.set_default("settings", "presence", (activity, title))
        await say(ctx, "Okay, presence updated.")

    @subcommand()
    @is_owner
    async def pull(self, ctx: sctx):
        """Performs git pull in the terminal."""
        await ctx.defer()
        try:
            if ".git" in os.listdir():
                p = subprocess.run("git pull",
                                   capture_output=True,
                                   check=True,
                                   shell=True,
                                   text=True)
                await say(ctx, p.stdout)
        except subprocess.CalledProcessError:
            await quip(ctx, "error")

    @subcommand()
    @option(description="The cog to update", required=True)
    @is_owner
    async def reload(self, ctx: sctx, cog: str):
        """Reloads a cog after pulling updates.

        Args:
            cog: The cog to reload.
        """
        if cog is not None:
            try:
                self.bot.reload("cogs." + cog)
                await quip(ctx, cog=cog)
            except Exception as e:
                print(f"{e.__class__.__name__}: {e}")
                await quip(ctx, "error")

    @subcommand()
    @option(description="The cog to load", required=True)
    @is_owner
    async def load(self, ctx: sctx, cog: str):
        """Load a new cog.

        Args:
            cog: The cog to load.
        """
        if cog is not None:
            try:
                self.bot.load("cogs." + cog)
                await quip(ctx, cog=cog)
            except Exception as e:
                print(f"{e.__class__.__name__}: {e}")
                await quip(ctx, "error")

    @subcommand()
    @option(description="The cog to unload", required=True)
    @is_owner
    async def unload(self, ctx: sctx, cog: str):
        """Unload an existing cog.

        Args:
            cog: The cog to unload.
        """
        if cog is not None:
            try:
                self.bot.remove("cogs." + cog)
                await quip(ctx, cog=cog)
            except Exception as e:
                print(f"{e.__class__.__name__}: {e}")
                await quip(ctx, "error")

    @subcommand()
    @is_owner
    async def list(self, ctx: sctx):
        """List all the commands in the bot."""
        commands = await get_command_list(self.bot)
        embed: Embed = Embed(
            title="Slash Commands:",
            description="\n".join(
                [command.replace(".", " ") for command in commands]))
        await ctx.send(embeds=embed)

    @subcommand()
    @is_owner
    async def exit(self, ctx: sctx) -> NoReturn:
        """Take a break and shut down."""
        await quip(ctx)
        print("Exit command received, see ya!")
        # TODO: Implement cleanup.
        raise KeyboardInterrupt


def setup(bot: Client):
    Manage(bot)
