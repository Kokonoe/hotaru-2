# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""Bot configuration module for the guild manager.

Settings are configured with the app.settings.Settings() object. A cog can be
configured to work with the Settings cog by adding a settings property to the
class.

    class MyCog(Extension):

        def __init__(self, bot: Client) -> None:
            self.bot: Client = bot
            self.settings: Settings = Settings(
                Setting("module_name", "setting_1", default_value),
                Setting("module_name", "setting_2", default_value)
            )
"""
from typing import cast
from interactions import (
    Client,
    CommandContext as sctx,
    Extension as InteractionsExtension,
    extension_command as base,
    option,
    Permissions as perm,
)

from app.settings import Setting as stg, Settings
from app.settings.validators import IntValidator
from helpers.basics import quip, say
from helpers.checks import has_permissions


class Extension(InteractionsExtension):

    def __init__(self) -> None:
        super().__init__()
        self.settings: Settings = Settings()


class Setting(InteractionsExtension):
    """The settings class allows the guild manager to manage settings."""

    def __init__(self, bot: Client) -> None:
        self.bot = bot
        self.settings: Settings = Settings(
            stg(self.__class__.__name__, "guild_permissions",
                perm.MANAGE_GUILD, IntValidator()))

    @base()
    async def setting(self, ctx: sctx) -> None:
        pass

    subcommand = setting.subcommand

    @subcommand()
    @option(description="The cog to configure", required=True)
    @option(description="The setting name", required=True)
    @option(description="The value to set", required=True)
    @has_permissions()
    async def set(self, ctx: sctx, cog: str, setting: str, value: str) -> None:
        """Set a setting.

        Args:
            cog: The name of the module.
            setting: The name of the key.
            value: The value to store.
        """
        bot_cog = cast(Extension, self.bot.get_extension(cog))
        settings: Settings = bot_cog.settings

        if isinstance(bot_cog, InteractionsExtension):
            bot_cog = self.bot.get_extension(cog.title())
            if bot_cog is None:
                await quip(ctx, "no_cog", cog=cog)
                return

        if hasattr(bot_cog, "settings"):
            if setting not in settings:
                await quip(ctx, "no_setting", cog=cog, setting=setting)
                return
            config: stg = settings[setting]
            value = await config.set(ctx.guild_id, value)
            await quip(ctx, cog=cog, setting=setting, value=value)
        else:
            await quip(ctx, "no_setting", cog=cog, setting=setting)

    @subcommand()
    @option(description="The cog to configure", required=True)
    @option(type=str, description="The setting name", required=False)
    @has_permissions()
    async def get(self,
                  ctx: sctx,
                  cog: str,
                  setting: str | None = None) -> None:
        """Get a setting or settings.

        Args:
            cog: The name of the module.
            setting: The name of the key. If omitted, return all the settings
                for the cog.
        """
        try:
            bot_cog = cast(Extension, self.bot.get_extension(cog))
        except AttributeError:
            # TODO: Actually log the incorrect cog name
            return

        settings: Settings = bot_cog.settings
        if isinstance(bot_cog, InteractionsExtension):
            bot_cog = self.bot.get_extension(cog.title())
            if bot_cog is None:
                await quip(ctx, "no_cog", cog=cog)
                return

        if hasattr(bot_cog, "settings"):
            if setting is not None:
                if setting not in settings:
                    await quip(ctx, "no_setting", cog=cog, setting=setting)
                    return
                else:
                    config: stg = settings[setting]
                    value = await config.get(ctx.guild_id)
                    await quip(ctx, cog=cog, setting=setting, value=value)
            else:
                entries: list[str] = []
                configs: list[stg] = [config for config in settings]
                for config in configs:
                    config: stg
                    key: str = config.key
                    value = await config.get(ctx.guild_id)
                    entries.append(f"{cog}.{key} : {value}")
                await say(ctx, "\n".join(entries))
        elif setting:
            await quip(ctx, "no_setting", cog=cog, setting=setting)

    @subcommand()
    @option(description="The cog to configure", required=True)
    @option(description="The setting name", required=True)
    @has_permissions()
    async def delete(self, ctx: sctx, cog: str, setting: str) -> None:
        """Delete a setting.

        Args:
            cog: The name of the module.
            setting: The name of the key.
        """
        bot_cog = cast(Extension, self.bot.get_extension(cog))
        settings: Settings = bot_cog.settings
        if isinstance(bot_cog, InteractionsExtension):
            bot_cog = self.bot.get_extension(cog.title())
            if bot_cog is None:
                await quip(ctx, "no_cog", cog=cog)
                return

        if hasattr(bot_cog, "settings"):
            if setting not in settings:
                await quip(ctx, "no_setting", cog=cog, setting=setting)
                return
            config: stg = settings[setting]
            result: bool = await config.delete(ctx.guild_id)
            if result:
                await quip(ctx, cog=cog, setting=setting)
        else:
            await quip(ctx, "no_setting", cog=cog, setting=setting)


def setup(bot: Client):
    Setting(bot)
