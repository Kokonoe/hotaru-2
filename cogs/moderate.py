# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""Moderation module for guild user management."""

from datetime import datetime, timedelta

from interactions import (
    CommandContext as sctx,
    Extension,
    LibraryException,
    extension_command as command,
    extension_message_command as message_command,
    Member,
    option,
    Snowflake,
)
import inflect

from app.logs.moderation import (
    get_moderation_entries_by_user,
    ModerationActionType,
    ModerationEntry,
    put_moderation_entry,
    update_moderation_entry,
)
from helpers.basics import Client, say

p = inflect.engine()


async def message_action(ctx: sctx) -> None:
    """Pins, unpins, or deletes a message."""
    assert ctx.data.target_id
    assert ctx.channel
    message_id: Snowflake = ctx.data.target_id
    message = await ctx.channel.get_message(message_id)

    match ctx.data.name:
        case "pin":
            await message.pin()
            reply = "Message pinned."
        case "unpin":
            await message.unpin()
            reply = "Message unpinned."
        case "delete":
            await message.delete()
            reply = "Message deleted."
        case _:
            await say(ctx, "Something borked.", hidden=True)
            return

    entry = ModerationEntry(None,
                            datetime.utcnow(),
                            message.content,
                            ModerationActionType[ctx.data.name.upper()],
                            str(ctx.user.id),
                            str(message.author.id),
                            original=str(message_id))
    await put_moderation_entry(str(ctx.guild_id), entry)
    await say(ctx, reply, hidden=True)


class Moderate(Extension):

    def __init__(self, bot: Client) -> None:
        self.bot: Client = bot

    @message_command()
    async def pin(self, ctx: sctx) -> None:
        """Pins a message."""
        await message_action(ctx)

    @message_command()
    async def unpin(self, ctx: sctx) -> None:
        """Unpins a message."""
        await message_action(ctx)

    @message_command(name="delete")
    async def delete_message(self, ctx: sctx) -> None:
        """Deletes a message."""
        await message_action(ctx)

    @command()  # type: ignore
    @option(description="The user to warn.",
            required=True)
    @option(description="The reason for the warn.",
            required=True)
    async def warn(self, ctx: sctx, user: Member, reason: str) -> None:
        """Issues a warning to a user."""
        entry = ModerationEntry(None, datetime.utcnow(), reason,
                                ModerationActionType.WARN, str(ctx.user.id),
                                str(user.id), True)
        resp = await put_moderation_entry(str(ctx.guild_id), entry)
        if resp:
            await say(ctx, f"User {user.name} warned.\nReason: {reason}")
        else:
            await say(ctx, "Something borked.")

    @command()  # type: ignore
    @option(description="The user to check.",
            required=True)
    async def warnlog(self, ctx: sctx, user: Member) -> None:
        """Gets the warnlog for a user."""
        entries = await get_moderation_entries_by_user(
            str(ctx.guild_id), str(user.id), ModerationActionType.WARN, True)
        if entries:
            warnings = "\n".join([
                f"{index + 1:02} | {entry.timestamp.strftime('%Y-%m-%d')}: {entry.note}"
                for index, entry in enumerate(entries)
            ])
            await say(ctx, f"**Warnlog for {user.name}:**\n{warnings}")
        else:
            await say(ctx, f"{user.name} looks clean... for now.")

    @command()  # type: ignore
    @option(description="The user to delete a warning from.",
            required=True)
    @option(description="The index of the warning to delete.",
            required=True)
    @option(description="The reason for the unwarn.",
            required=True)
    async def unwarn(self, ctx: sctx, user: Member, index: int,
                     reason: str) -> None:
        """Removes a warning from a user.

        Use /warnlog to find the warning indices.
        """
        entries = await get_moderation_entries_by_user(
            str(ctx.guild_id), str(user.id), ModerationActionType.WARN, True)
        if entries and 1 <= index <= len(entries):
            entry = entries[index - 1]
            entry.active = False
            unwarn_record = ModerationEntry(None,
                                            datetime.utcnow(),
                                            reason,
                                            ModerationActionType.UNWARN,
                                            str(ctx.user.id),
                                            str(user.id),
                                            original=entry.record_no)
            if (await update_moderation_entry(str(ctx.guild_id), entry) and
                    await put_moderation_entry(str(ctx.guild_id),
                                               unwarn_record)):
                await say(
                    ctx,
                    f"Warning #{index:02} for user {user.name} deleted.\nReason: {reason}"
                )
            else:
                await say(ctx, "Something went wrong with the deletion...")
        else:
            await say(ctx, "I don't see that one.")

    @command()  # type: ignore
    @option(description="The user to kick.",
            required=True)
    @option(description="The reason for the kick.",
            required=True)
    async def kick(self, ctx: sctx, user: Member, reason: str) -> None:
        """Kicks a user."""
        entry = ModerationEntry(None, datetime.utcnow(), reason,
                                ModerationActionType.KICK, str(ctx.user.id),
                                str(user.id))
        await user.kick(ctx.guild_id, reason)
        resp = await put_moderation_entry(str(ctx.guild_id), entry)
        if resp:
            await say(ctx, f"User {user.name} kicked.\nReason: {reason}")
        else:
            await say(ctx, "Something borked.")

    @command()  # type: ignore
    @option(description="The user to timeout.",
            required=True)
    @option(description="The reason for the timeout.",
            required=True)
    @option(type=int, name="days", description="Days to timeout.")
    @option(type=int, name="hours", description="Hours to timeout.")
    @option(type=int, name="minutes", description="Minutes to timeout.")
    async def timeout(self,
                      ctx: sctx,
                      user: Member,
                      reason: str,
                      days: int = 0,
                      hours: int = 0,
                      minutes: int = 0) -> None:
        """Issues a timeout to a user."""
        if (not days and not hours and
                not minutes) or days < 0 or hours < 0 or minutes < 0:
            await say(ctx, "Funny.")
            return
        entry = ModerationEntry(None, datetime.utcnow(), reason,
                                ModerationActionType.TIMEOUT, str(ctx.user.id),
                                str(user.id))
        expiration = datetime.utcnow() + (duration := timedelta(
            days=days, hours=hours, minutes=minutes))
        try:
            await user.modify(
                ctx.guild_id,
                communication_disabled_until=expiration.isoformat())
        except LibraryException as e:
            print(e)
            await say(ctx, "Timeout failed.")
            return
        resp = await put_moderation_entry(str(ctx.guild_id), entry)
        if resp:
            hours, remainder = divmod(duration.seconds, 3600)
            minutes = remainder // 60
            duration_string: list[str] = []
            if duration.days > 0:
                duration_string.append(
                    f"{duration.days} {p.plural('day', duration.days)}")
            if hours or minutes:
                duration_string.append(f"{hours:02}:{minutes:02}")
            await say(
                ctx,
                f"User {user.name} timed out for {' and '.join(duration_string)}.\nReason: {reason}"
            )
        else:
            await say(ctx, "Something borked.")

    @command()  # type: ignore
    @option(description="The reason for the timeout.",
            required=True)
    async def untimeout(self, ctx: sctx, user: Member, reason: str) -> None:
        """Revokes a timeout, allowing the user to communicate."""
        entry = ModerationEntry(None, datetime.utcnow(),
                                reason, ModerationActionType.UNTIMEOUT,
                                str(ctx.user.id), str(user.id))
        try:
            await user.modify(ctx.guild_id, communication_disabled_until=None)
        except LibraryException as e:
            print(e)
            await say(ctx, "Untimeout failed.")
            return
        resp = await put_moderation_entry(str(ctx.guild_id), entry)
        if resp:
            await say(ctx, f"User {user.name} un-timed-out.\nReason: {reason}")
        else:
            await say(ctx, "Something borked.")


def setup(bot: Client):
    Moderate(bot)
