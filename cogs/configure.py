# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""Slash Command permissions management for the guild."""

from asyncio import sleep, TimeoutError

from interactions import (
    ActionRow,
    Button,
    Channel,
    Choice,
    CommandContext as sctx,
    ComponentContext as cctx,
    Extension,
    extension_command as base,
    extension_component as component,
    Member,
    Message,
    option,
    OptionType as opt,
    Permissions as gperm,
    Role,
)

from app.conf import scope
from app.settings import Setting, Settings, IntValidator
from helpers.basics import Client, get_command_list, make_buttons, quip
from helpers.checks import has_permissions
from helpers.permissions import CommandPermissions as cperm


class Configure(Extension):
    """The configure class manages Slash Command permissions for the guild.

    A more descriptive name might have been Permissions, but there plenty of
    modules with that name already.
    """

    def __init__(self, bot: Client) -> None:
        self.bot = bot
        self.settings: Settings = Settings(
            Setting(self.__class__.__name__, "guild_permissions",
                    gperm.MANAGE_GUILD, IntValidator()))

    @base(scope=scope)
    async def config(self, ctx: sctx):
        pass

    @config.subcommand()
    @has_permissions()
    async def init(self, ctx: sctx) -> None:
        """Initializes the permissions for the server."""
        buttons: dict[str, Button] = await make_buttons(ctx, "init", "yes",
                                                        "no")
        message: Message = await quip(
            ctx, components=ActionRow.new(*buttons.values()))

        async def check(bctx: cctx) -> bool:
            assert bctx.author
            if bctx.message is None or bctx.message.interaction is None:
                return False
            return True if (bctx.message.interaction is not None and
                            bctx.message.interaction.user is not None and
                            str(bctx.author.id) == str(
                                bctx.message.interaction.user.id)) else False

        if message:
            try:
                await self.bot.wait_for_component(
                    components=["init.yes", "init.no"], check=check, timeout=15)
            except TimeoutError:
                await message.delete()

    async def init_component(self, ctx: cctx) -> None:
        assert ctx.message
        assert ctx.author
        if (ctx.message.interaction is not None and
                ctx.message.interaction.user is not None and
                str(ctx.author.id) == str(ctx.message.interaction.user.id)):
            assert ctx.data.custom_id
            choice: str = ctx.data.custom_id
            if choice == "init.yes":
                await cperm(ctx).init()
                await quip(ctx, "yes")
            elif choice == "init.no":
                await quip(ctx, "no")
        else:
            message: Message = ctx.message
            await quip(ctx, "stranger", message=message)
            await sleep(2)
            await quip(ctx, "anyway", message=message)
            await sleep(1)
            await quip(ctx, "original", message=message)

    @component("init.yes")  # TODO: startswith=True
    async def init_component_yes(self, ctx: cctx) -> None:
        await self.init_component(ctx)

    @component("init.no")
    async def init_component_no(self, ctx: cctx) -> None:
        await self.init_component(ctx)

    @config.subcommand()
    @option(description="The command to clear.",
            required=True,
            autocomplete=True)
    @option(description="Base permission settings.",
            required=True,
            choices=[
                Choice(name="restrict", value="restrict"),
                Choice(name="permit", value="permit")
            ])
    @has_permissions()
    async def clear(self, ctx: cctx, command: str, base: str) -> None:
        """Clear the settings from a given command.

        Args:
            command: The command to clear permissions from.
        """
        permissions: cperm = cperm(ctx, command)
        if base == "restrict":
            await permissions.clear().restrict().set()
        elif base == "permit":
            await permissions.clear().permit().set()
        await quip(ctx, command=command)

    @config.subcommand()
    @option(description="Whether to enable use of the command.", required=True)
    @option(description="The command to disable.",
            required=True,
            autocomplete=True)
    @has_permissions()
    async def command(self, ctx: cctx, enable: bool, command: str) -> None:
        """Enable or disable commands in the guild.

        Args:
            enable: Enable the command if True.
            command: The command to enable or disable.
        """
        permissions: cperm = cperm(ctx, command)
        await permissions.get()
        permissions.perms.enabled = enable
        await permissions.set()
        await quip(ctx, "enabled" if enable else "disabled", command=command)

    @config.subcommand()
    @option(description="The operation to perform.",
            required=True,
            choices=[
                Choice(name="allow", value="allow"),
                Choice(name="disallow", value="disallow"),
                Choice(name="block", value="block"),
                Choice(name="unblock", value="unblock")
            ])
    @option(description="The command to set.", required=True, autocomplete=True)
    @option(type=opt.MENTIONABLE,
            description="The role or member.",
            required=True)
    @has_permissions()
    async def member(self, ctx: cctx, action: str, command: str,
                     member: Member | Role) -> None:
        """Edit user and role permissions for a command.

        Args:
            action:
                allow: Explicitly allow use of the command.
                disallow: Remove from the allowed list.
                block: Explicitly block from the command.
                unblock: Remove from the blocked list.
            command: The command to edit permissions for.
            member: A user or role.
        """
        permissions: cperm = cperm(ctx, command)
        await permissions.get()
        member_id: str = str(member.id)
        if isinstance(member, Member):
            assert member.user
            member_string: str = (
                f"{member.nick or member.user.username}#{member.user.discriminator}"
            )
        else:
            member_string: str = f"{member.name}"
        try:
            if isinstance(member, Member):
                if action == "allow":
                    permissions.perms.users_whitelist.add(member_id)
                elif action == "disallow":
                    permissions.perms.users_whitelist.remove(member_id)
                elif action == "block":
                    permissions.perms.users_blacklist.add(member_id)
                elif action == "unblock":
                    permissions.perms.users_blacklist.remove(member_id)
            elif isinstance(member, Role):
                if action == "allow":
                    permissions.perms.roles_whitelist.add(member_id)
                elif action == "disallow":
                    permissions.perms.roles_whitelist.remove(member_id)
                elif action == "block":
                    permissions.perms.roles_blacklist.add(member_id)
                elif action == "unblock":
                    permissions.perms.roles_blacklist.remove(member_id)
            await permissions.set()
            await quip(ctx,
                       member=member_string,
                       action=action,
                       to="to" if action == "allow" else "from",
                       command=command)
        except KeyError:
            await quip(ctx,
                       "key_error",
                       member=member_string,
                       action=action,
                       command=command)

    @config.subcommand()
    @option(description="The operation to perform.",
            required=True,
            choices=[
                Choice(name="allow everywhere", value="allow_all"),
                Choice(name="block everywhere", value="block_all"),
                Choice(name="allow", value="allow"),
                Choice(name="disallow", value="disallow"),
                Choice(name="block", value="block"),
                Choice(name="unblock", value="unblock")
            ])
    @option(description="The command to set.", required=True, autocomplete=True)
    @option(type=opt.CHANNEL, description="The channel", required=False)
    @has_permissions()
    async def channel(self,
                      ctx: cctx,
                      action: str,
                      command: str,
                      channel: Channel | None = None) -> None:
        """Edit channel permissions for a command.

        Args:
            action:
                allow everywhere: Allow the command in all channels unless
                    explicitly blocked.
                block everywhere: Block the command in all channels unless
                    explicitly allowed.
                allow: Explicitly allow the command in the channel.
                disallow: Remove channel from the allowed list.
                block: Explicitly block the command in the channel.
                unblock: Remove channel from the blocked list.
            command: The command to edit permissions for.
            channel: A guild channel.
        """
        if channel is None:
            return
        permissions: cperm = cperm(ctx, command)
        await permissions.get()
        channel_id = str(channel.id)
        try:
            if action == "allow_all":
                permissions.perms.channels_allow_all = True
            elif action == "block_all":
                permissions.perms.channels_allow_all = False
            elif action == "allow":
                permissions.perms.channels_whitelist.add(channel_id)
            elif action == "disallow":
                permissions.perms.channels_whitelist.remove(channel_id)
            elif action == "block":
                permissions.perms.channels_blacklist.add(channel_id)
            elif action == "unblock":
                permissions.perms.channels_blacklist.remove(channel_id)
            await permissions.set()
            if action in ["allow_all", "block_all"]:
                action_string = ("allowed in"
                                 if action == "allow_all" else "blocked from")
                await quip(ctx, "all", command=command, action=action_string)
            else:
                await quip(ctx,
                           channel=channel.name,
                           action=action,
                           to="to" if action == "allow" else "from",
                           command=command)
        except KeyError:
            await quip(ctx,
                       "key_error",
                       channel=channel.name,
                       action=action,
                       command=command)

    @config.autocomplete("command")
    async def command_command_autocomplete(self, ctx: sctx, input: str = ""):
        globals: list[str] = await get_command_list(self.bot, base_only=True)
        locals: list[str] = await get_command_list(self.bot,
                                                   ctx.guild_id,
                                                   base_only=True)

        commands: list[str] = sorted(
            filter(lambda x: input in x, set(globals + locals)))

        await ctx.populate(
            [Choice(name=command, value=command) for command in commands[:25]
            ] or [Choice(name="No matches found.", value="")])


def setup(bot: Client):
    Configure(bot)
