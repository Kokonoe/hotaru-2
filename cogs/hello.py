# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""Fun social interactions."""

from interactions import (
    ApplicationCommandType as act,
    Client,
    CommandContext as sctx,
    EmbedAuthor,
    Extension,
    extension_command as command,
    Member,
    option,
)
from app import conf
from helpers.basics import Embed, quip, say


class Hello(Extension):
    """The hello class adds some fun interactions to the guild."""

    def __init__(self, bot: Client):
        self.bot = bot

    @command()
    async def ping(self, ctx: sctx):
        """Check Hotaru's latency."""
        await say(ctx, f"Pong! {self.bot.latency:.0f}ms", hidden=True)

    @command()
    async def hello(self, ctx: sctx):
        """Hotaru says hello!"""
        assert ctx.member

        if str(ctx.member.id) in conf.owners:
            await quip(ctx)
        else:
            await quip(ctx, "hostile")

    @command(type=act.USER)
    async def compliment(self, ctx: sctx):
        """Compliment the target user."""
        if isinstance(ctx.target, Member):
            target: Member = ctx.target
            assert target.user
            embed: Embed = Embed(
                description="You look great",
                author=EmbedAuthor(
                    name=target.nick if target.nick else target.user.username,
                    icon_url=target.user.avatar_url,
                ),
            )
            await ctx.send(embeds=embed)

    @command()  # type: ignore
    @option(description="The message to repeat", required=True)
    async def say(self, ctx: sctx, message: str):
        """Hotaru repeats a message.

        Args:
            message: The message to repeat.
        """
        await say(ctx, message)


def setup(bot: Client):
    Hello(bot)
