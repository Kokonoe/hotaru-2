# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""A dictionary commands module."""

from interactions import (
    Choice,
    Client,
    CommandContext as sctx,
    Extension,
    extension_command as base,
    option,
    Permissions as perm,
)
from pymongo.results import (
    DeleteResult as dres,
    UpdateResult as ures,
)

from app.database import settings_service as service
from app.settings import IntValidator, Setting, Settings
from helpers.basics import Embed, quip
from helpers.checks import has_permissions


class Dictionary(Extension):
    """The dictionary class manages the guild dictionary.

    The help dictionary contains a guild's help messages. The user dictionary
    contains fun entries about a guild's users.
    """

    def __init__(self, bot: Client) -> None:
        self.bot = bot
        self.settings = Settings(
            Setting(self.__class__.__name__, "guild_permissions",
                    perm.MANAGE_MESSAGES, IntValidator()))

    @base(name="dict")
    async def dictionary(self, ctx: sctx) -> None:
        pass

    subcommand = dictionary.subcommand

    @subcommand()
    @option(description="The user or help category.",
            required=True,
            choices=[
                Choice(name="user", value="user"),
                Choice(name="help", value="help"),
            ])
    @option(description="The key for this entry.", required=True)
    @option(description="The value for this entry.", required=True)
    @has_permissions()
    async def put(self, ctx: sctx, cat: str, key: str, value: str) -> None:
        """Add a new dictionary entry or replace an existing one.

        Args:
            cat: The user or help dictionary.
            key: The entry's key.
            value: The entry's value.
        """
        if cat not in ["user", "help"]:
            await quip(ctx, "cat")
            return
        doc = f"{cat}_dict"
        result: ures = await service.replace(ctx.guild_id, doc, key, value)
        if result.acknowledged:
            await quip(ctx, key=key)
        else:
            await quip(ctx, "error")

    @subcommand()
    @option(description="The key for this entry.", required=True)
    @has_permissions()
    async def get(self, ctx: sctx, key: str) -> None:
        """Return the value for a dictionary key.

        Checks the help dictionary, first, and then falls back to the user
        dictionary as an easter egg backup.

        Args:
            key: The entry's key.
        """
        help_doc = await service.get(ctx.guild_id, "help_dict", key)
        if help_doc is not None:
            embed = Embed(description=help_doc, title=key)
            await ctx.send(embeds=embed)
        else:
            user_doc = await service.get(ctx.guild_id, "user_dict", key)
            if user_doc is not None:
                embed = Embed(description=user_doc, title=key)
                await ctx.send(embeds=embed)
            else:
                await quip(ctx, "error")

    @subcommand()
    @option(description="The user or help category.",
            required=True,
            choices=[
                Choice(name="user", value="user"),
                Choice(name="help", value="help"),
            ])
    @option(description="The key for the entry to remove.", required=True)
    @has_permissions()
    async def delete(self, ctx: sctx, cat: str, key: str) -> None:
        """Delete a dictionary entry.

        Args:
            cat: The user or help dictionary.
            key: The entry's key.
        """
        if cat not in ["user", "help"]:
            await quip(ctx, "cat")
            return
        doc = f"{cat}_dict"
        result: dres = await service.delete(ctx.guild_id, doc, key)
        if result.deleted_count > 0:
            await quip(ctx, key=key.lower())
        else:
            await quip(ctx, "error")

    @subcommand()
    @option(description="The user or help category.",
            required=True,
            choices=[
                Choice(name="user", value="user"),
                Choice(name="help", value="help"),
            ])
    @has_permissions()
    async def list(self, ctx: sctx, cat: str) -> None:
        """List all known dictionary keys.

        Args:
            cat: The user or help dictionary.
        """
        if cat not in ["user", "help"]:
            await quip(ctx, "cat")
            return
        doc = f"{cat}_dict"
        documents = await service.list_keys(ctx.guild_id, doc)
        keys = [document for document in documents.keys()]
        if len(keys) > 0:
            embed = Embed(description=", ".join(sorted(keys)),
                          title="Dictionary Keys")
            await ctx.send(embeds=embed)
        else:
            await quip(ctx, "error")


def setup(bot: Client):
    Dictionary(bot)
