# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""Experience management."""

from __future__ import annotations

from datetime import datetime, timedelta

from interactions import (
    CommandContext as sctx,
    Extension,
    extension_command as base,
    extension_listener as listener,
    Guild,
    HTTPClient,
    Member,
    Message,
    option,
    OptionType as opt,
    Role,
)

from app import conf
from app.logs.experience import (
    ExperienceActionType as etype,
    ExperienceEntry as eentry,
)
from app.settings import Setting, Settings
from app.settings import IntValidator, FloatValidator
from app.users import experience
from helpers.basics import Client, say
import app.logs.experience as elogs

XP_MULTIPLIER = "xp_multiplier"
XP_GROWTH = "xp_growth_ratio"
XP_COOLDOWN = "xp_cooldown_seconds"
XP_REST = "xp_rest_bonus_interval_seconds"
XP_BONUS = "xp_rest_bonus_multiplier"
XP_ROLES = "xp_roles"


class Experience(Extension):

    def __init__(self, bot: Client):
        self.bot: Client = bot
        self.settings: Settings = Settings(
            Setting(self.__class__.__name__, XP_MULTIPLIER, 5,
                    IntValidator(0, convert=True)),
            Setting(self.__class__.__name__, XP_GROWTH, 1.0,
                    FloatValidator(0, convert=True)),
            Setting(self.__class__.__name__, XP_COOLDOWN, 60,
                    IntValidator(0, convert=True)),
            Setting(self.__class__.__name__, XP_REST, 3600,
                    IntValidator(0, convert=True)),
            Setting(self.__class__.__name__, XP_BONUS, 5,
                    IntValidator(1, convert=True)),
            Setting(self.__class__.__name__, XP_ROLES, {}))

    @base(scope=conf.scope)
    async def xp(self, ctx: sctx) -> None:
        pass

    subcommand = xp.subcommand

    @subcommand()
    @option(type=opt.INTEGER,
            description="The amount of XP to increment.",
            required=True)
    @option(type=opt.USER, description="The user to increment.", required=True)
    @option(type=opt.STRING,
            description="The reason for the increase.",
            required=True)
    async def increment(self, ctx: sctx, xp_inc: int, user: Member,
                        note: str) -> None:
        """Add XP to a user's balance."""
        assert ctx
        assert ctx.author

        value = await experience.increment(str(ctx.guild_id), str(user.id),
                                           xp_inc)
        log: eentry = eentry(None, datetime.utcnow(), note, etype.AWARD,
                             str(ctx.author.id), str(user.id), xp_inc, value)
        await elogs.put_experience_entry(str(ctx.guild_id), log)
        await say(ctx, f"User {user.name} xp incremented to {value}.")

    @subcommand()
    @option(type=opt.USER, description="The user to query.", required=False)
    async def query(self, ctx: sctx, user: Member | None = None) -> None:
        """Check a user's level."""
        assert ctx.author

        if user is None:
            user = ctx.author
        xp = await experience.get(str(ctx.guild_id), str(user.id))
        multiplier: int = await self.settings[XP_MULTIPLIER](ctx.guild_id)
        ratio: float = await self.settings[XP_GROWTH](ctx.guild_id)
        level = experience.calculate_level(multiplier, ratio, xp)
        await say(ctx, f"User {user.name} is level {level} ({xp} xp).")

    @subcommand()
    @option(type=opt.ROLE, description="The role to assign.", required=True)
    @option(type=opt.INTEGER,
            description="the level to assign the role.",
            required=True)
    async def set_role_level(self, ctx: sctx, role: Role, level: int) -> None:
        """Set roles to give at certain levels."""
        setting: Setting = self.settings[XP_ROLES]
        xp_roles: dict[str, int] = roles if (roles := await setting.get(
            ctx.guild_id)) else {}
        xp_roles[str(role.id)] = level
        await setting.set(ctx.guild_id, xp_roles)
        await say(ctx, f"Role {role.name} will be assigned at level {level}.")

    @subcommand()
    @option(type=opt.ROLE, description="The role to unassign.", required=True)
    async def unset_role_level(self, ctx: sctx, role: Role) -> None:
        """Remove a level role."""
        setting: Setting = self.settings[XP_ROLES]
        xp_roles: dict[str, int] = roles if (roles := await setting.get(
            ctx.guild_id)) else {}
        xp_roles.pop(str(role.id), None)
        await setting.set(ctx.guild_id, xp_roles)
        await say(ctx, f"Role {role.name} will not be assigned.")

    @listener(name="on_message_create")  # type: ignore
    async def xp_handler(self, message: Message) -> None:
        """The xp handler updates experience and roles on message.

        It calculates xp to reward based on the guild's xp rate settings, awards
        the member with any xp-gated roles, and records these roles for later
        granting if the member leaves and re-joins.

        Args:
            message (Message)
        """
        if message.author.bot:
            return

        assert isinstance(self.bot._http, HTTPClient)

        message._client = self.bot._http

        guild_id: str = str(message.guild_id)
        guild: Guild = await message.get_guild()
        user_id: str = str(message.author.id)
        member: Member = await guild.get_member(int(user_id))
        member_roles: list[int] = member.roles

        multiplier: int = await self.settings[XP_MULTIPLIER](guild_id)
        growth: float = await self.settings[XP_GROWTH](guild_id)
        cooldown: int = await self.settings[XP_COOLDOWN](guild_id)
        rest_interval: int = await self.settings[XP_REST](guild_id)
        bonus: int = await self.settings[XP_BONUS](guild_id)
        xp_roles: dict[str, int] = await self.settings[XP_ROLES](guild_id)

        async with await experience.client.start_session() as session:
            async with session.start_transaction():
                # Check cooldown and calculate xp.
                recent_count: int
                most_recent: datetime | None
                recent_count, most_recent = await experience.log_timestamp(
                    guild_id,
                    user_id,
                    cooldown,
                    datetime.utcnow(),
                    session=session)
                if most_recent is None or (datetime.utcnow() -
                                           most_recent) >= timedelta(
                                               seconds=rest_interval):
                    awarded_xp = multiplier * bonus
                else:
                    awarded_xp = max(multiplier - recent_count, 1)

                # Award xp and calculate level.
                xp = await experience.increment(guild_id,
                                                user_id,
                                                awarded_xp,
                                                session=session)
                level = experience.calculate_level(multiplier, growth, xp)

                # Check for roles.
                roles_to_add: set[int] = set()
                for role_id, role_level in xp_roles.items():
                    if level >= role_level:
                        roles_to_add.add(int(role_id))
                        if role_id not in member_roles:
                            await member.add_role(int(role_id))
                await experience.add_roles(guild_id,
                                           user_id,
                                           *roles_to_add,
                                           session=session)


def setup(bot: Client):
    Experience(bot)
