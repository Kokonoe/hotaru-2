# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury

from copy import copy
from datetime import datetime
from typing import Any

import pytest

from app.logs import ModerationActionType, ModerationEntry
from app.logs.moderation import ModerationEntryDict
from app.settings import StrValidator

pytestmark = pytest.mark.asyncio

pad: StrValidator = StrValidator(16)
GUILD_ID: str = pad.cast(0)
MODERATOR_ID: str = pad.cast(1111111111111111)
USER_ID: str = pad.cast(2222222222222222)
REASON: str = "Warmed"
NOW: datetime = datetime.utcnow().replace(microsecond=0)
ENTRY_1: ModerationEntry = ModerationEntry(None, NOW, REASON,
                                           ModerationActionType.WARN,
                                           MODERATOR_ID, USER_ID)
ENTRY_2: ModerationEntry = ModerationEntry(None, NOW, REASON,
                                           ModerationActionType["KICK"],
                                           MODERATOR_ID, USER_ID)


async def test_put(logs_client):
    result_1 = result_2 = None
    async with await logs_client.start_session() as session:
        async with session.start_transaction():
            entry_1 = copy(ENTRY_1)
            entry_2 = copy(ENTRY_2)
            result_1 = await entry_1.put(GUILD_ID, session)
            result_2 = await entry_2.put(GUILD_ID, session)
            await session.abort_transaction()
    assert result_1 and result_2


async def test_get(logs_client):
    results = []
    async with await logs_client.start_session() as session:
        key: dict[str, Any] = {"moderator_id": MODERATOR_ID}
        async with session.start_transaction():
            entry_1 = copy(ENTRY_1)
            entry_2 = copy(ENTRY_2)
            await entry_1.put(GUILD_ID, session)
            await entry_2.put(GUILD_ID, session)
            results: list[ModerationEntry] = await ModerationEntry.get(
                GUILD_ID, key, session)
            await session.abort_transaction()
    expected: list[ModerationEntryDict] = [
        ENTRY_1.to_dict(), ENTRY_2.to_dict()
    ]
    for result in results:
        entry: ModerationEntryDict = result.to_dict()
        entry.pop("_id")
        assert entry in expected


async def test_delete(logs_client):
    result = None
    key = {"user_id": USER_ID}
    async with await logs_client.start_session() as session:
        async with session.start_transaction():
            entry_2 = copy(ENTRY_2)
            await entry_2.put(GUILD_ID, session)
            result = await ModerationEntry.delete(GUILD_ID, key, session)
            await session.abort_transaction()
    assert result
