from datetime import datetime

import pytest

from app.users import experience

MULTIPLIER = 5
GROWTH = 1
GUILD_ID = "1234567890123456"
USER_ID = "1111222233334444"

pytestmark = pytest.mark.asyncio


def test_calculate_level():
    assert experience.calculate_level(MULTIPLIER, GROWTH, 40) == 2


async def test_database_transactions(users_client):
    XP = 58
    response = None
    post_transaction_response = None

    async with await users_client.start_session() as session:
        async with session.start_transaction():
            await experience.put(GUILD_ID, USER_ID, XP, session=session)
            response = await experience.get(GUILD_ID, USER_ID, session=session)
            await session.abort_transaction()
        post_transaction_response = await experience.get(GUILD_ID,
                                                         USER_ID,
                                                         session=session)

    assert XP == response
    assert post_transaction_response is None


async def test_increment(users_client):
    XP_INC = 5
    TIMES_TO_REPEAT = 5
    xp = None

    async with await users_client.start_session() as session:
        async with session.start_transaction():
            for _ in range(TIMES_TO_REPEAT):
                await experience.increment(GUILD_ID,
                                           USER_ID,
                                           XP_INC,
                                           session=session)
            xp = await experience.get(GUILD_ID, USER_ID, session=session)
            await session.abort_transaction()

    assert xp == XP_INC * TIMES_TO_REPEAT


async def test_history(users_client):
    TIMESTAMPS = [
        datetime(2023, 1, 18, 18, 41, 0),
        datetime(2023, 1, 18, 18, 41, 10),
        datetime(2023, 1, 18, 18, 41, 20),
        datetime(2023, 1, 18, 18, 41, 30),
        datetime(2023, 1, 18, 18, 41, 40),
        datetime(2023, 1, 18, 18, 41, 50),
        datetime(2023, 1, 18, 18, 42, 0)
    ]
    response = None

    async with await users_client.start_session() as session:
        async with session.start_transaction():
            await experience.put_history(GUILD_ID,
                                         USER_ID,
                                         TIMESTAMPS,
                                         session=session)
            response = await experience.get_history(GUILD_ID,
                                                    USER_ID,
                                                    session=session)
            await session.abort_transaction()

    for i in range(len(TIMESTAMPS)):
        assert response[i] == TIMESTAMPS[i]


async def test_log_timestamp(users_client):
    ENTRIES = 5
    INTERVAL = 60
    count = 0

    async with await users_client.start_session() as session:
        async with session.start_transaction():
            for i in range(ENTRIES):
                count, _ = await experience.log_timestamp(GUILD_ID,
                                                          USER_ID,
                                                          INTERVAL,
                                                          datetime.utcnow(),
                                                          session=session)
            await session.abort_transaction()

    assert count == ENTRIES - 1
