# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury

from asyncio import AbstractEventLoop as loop, get_event_loop
from typing import Iterator

import pytest

from app.database import (
    settings_service as sservice,
    users_service as uservice,
    logs_service as lservice,
)


@pytest.fixture(scope="module")
def event_loop() -> Iterator[loop]:
    """An override of the default pytest event_loop with a widened scope."""
    loop = get_event_loop()
    yield loop
    loop.close()


@pytest.fixture
def settings_client(event_loop: loop):
    """Initializes the settings service with pytest's event_loop.

    Args:
        event_loop (AbstractEventLoop):
            Event loop fixture provided by pytest-asyncio

    Returns:
        AsyncIOMotorClient
    """
    client = sservice.db.client
    client._io_loop = event_loop
    return sservice.db.client


@pytest.fixture
def users_client(event_loop: loop):
    """Initializes the users service with pytest's event_loop.

    Args:
        event_loop (AbstractEventLoop):
            Event loop fixture provided by pytest-asyncio

    Returns:
        AsyncIOMotorClient
    """
    client = uservice.db.client
    client._io_loop = event_loop
    return uservice.db.client


@pytest.fixture
def logs_client(event_loop: loop):
    """Initializes the logs service with pytest's event_loop.

    Args:
        event_loop (AbstractEventLoop):
            Event loop fixture provided by pytest-asyncio

    Returns:
        AsyncIOMotorClient
    """
    client = lservice.db.client
    client._io_loop = event_loop
    return lservice.db.client
