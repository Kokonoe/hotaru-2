# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury

import pytest

from app.database import users_service as service

GUILD_ID = "000000000000000000"
DOC = "000000000000000000"
KEY = "experience"
VALUE = 30

# * Decorates the entire module's functions as coroutines!
pytestmark = pytest.mark.asyncio


async def test_put(users_client):
    result = None
    async with await users_client.start_session() as session:
        async with session.start_transaction():
            result = await service.replace(GUILD_ID, DOC, KEY, VALUE, session)
            await session.abort_transaction()
    assert result.upserted_id is not None


async def test_get(users_client):
    result = None
    async with await users_client.start_session() as session:
        async with session.start_transaction():
            await service.replace(GUILD_ID, DOC, KEY, VALUE, session)
            result = await service.get(GUILD_ID, DOC, KEY, session)
            await session.abort_transaction()
    assert result == VALUE


async def test_increment(users_client):
    result = None
    async with await users_client.start_session() as session:
        async with session.start_transaction():
            await service.increment(GUILD_ID, DOC, KEY, VALUE, session)
            result = await service.increment(GUILD_ID, DOC, KEY, VALUE,
                                             session)
            await session.abort_transaction()
    assert result == VALUE * 2


async def test_empty_get(users_client):
    result = None
    async with await users_client.start_session() as session:
        async with session.start_transaction():
            await service.replace(GUILD_ID, DOC, KEY, VALUE, session)
            result = await service.get(GUILD_ID, DOC, "invalid_key", session)
            await session.abort_transaction()
    assert result is None


async def test_delete(users_client):
    result = None
    async with await users_client.start_session() as session:
        async with session.start_transaction():
            await service.replace(GUILD_ID, DOC, KEY, VALUE, session)
            result = await service.delete(GUILD_ID, DOC, KEY, session)
            await session.abort_transaction()
    assert result.deleted_count == 1


async def test_list(users_client):
    result = None
    async with await users_client.start_session() as session:
        async with session.start_transaction():
            await service.replace(GUILD_ID, DOC, KEY, VALUE, session)
            result = await service.list_keys(GUILD_ID, DOC, session)
            await session.abort_transaction()
    assert len(result) > 0
