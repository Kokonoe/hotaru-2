# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury

from motor.motor_asyncio import AsyncIOMotorDatabase

from app.database.connection import db


def test_get_connection():
    assert isinstance(db.settings, AsyncIOMotorDatabase)
