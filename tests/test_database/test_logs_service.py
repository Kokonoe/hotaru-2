# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury

from copy import copy
from datetime import datetime

import pytest

from app.database import logs_service as service

GUILD_ID = "000000000000000000"
DOC = "experience"
KEY = "gain"
NOW = datetime.utcnow().replace(microsecond=0)

ENTRY_1 = {
    "key": KEY,
    "value": 10,
    "timestamp": NOW,
}
ENTRY_2 = {
    "key": KEY,
    "value": 20,
    "timestamp": NOW,
}

# * Decorates the entire module's functions as coroutines!
pytestmark = pytest.mark.asyncio


async def test_put(logs_client):
    result = None
    async with await logs_client.start_session() as session:
        async with session.start_transaction():
            result = await service.put_one(GUILD_ID, DOC, copy(ENTRY_1),
                                           session)
            await session.abort_transaction()
    assert result.inserted_id is not None


async def test_get(logs_client):
    results = None
    async with await logs_client.start_session() as session:
        async with session.start_transaction():
            await service.put_one(GUILD_ID, DOC, copy(ENTRY_1), session)
            await service.put_one(GUILD_ID, DOC, copy(ENTRY_2), session)
            results = await service.get_many(GUILD_ID, DOC, {"key": KEY},
                                             session)
            await session.abort_transaction()
    expected = [ENTRY_1, ENTRY_2]
    for document in results:
        document.pop("_id")
        assert document in expected


async def test_empty_get(logs_client):
    results = None
    async with await logs_client.start_session() as session:
        async with session.start_transaction():
            await service.put_one(GUILD_ID, DOC, copy(ENTRY_1), session)
            await service.put_one(GUILD_ID, DOC, copy(ENTRY_2), session)
            results = await service.get_many(GUILD_ID, DOC,
                                             {"key": "invalid_key"}, session)
            await session.abort_transaction()
    assert not results


async def test_delete(logs_client):
    result = None
    async with await logs_client.start_session() as session:
        async with session.start_transaction():
            await service.put_one(GUILD_ID, DOC, copy(ENTRY_1), session)
            result = await service.delete_one(GUILD_ID, DOC, {"key": KEY},
                                              session)
            await session.abort_transaction()
    assert result.deleted_count == 1
