# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury

import pytest

from app.database import settings_service as service

GUILD_ID = "000000000000000000"
DOC = "user_dict"
KEY = "python"
VALUE = "snek"

# * Decorates the entire module's functions as coroutines!
pytestmark = pytest.mark.asyncio


async def test_put(settings_client):
    result = None
    async with await settings_client.start_session() as session:
        async with session.start_transaction():
            result = await service.replace(GUILD_ID, DOC, KEY, VALUE, session)
            await session.abort_transaction()
    assert result.upserted_id is not None


async def test_get(settings_client):
    result = None
    async with await settings_client.start_session() as session:
        async with session.start_transaction():
            await service.replace(GUILD_ID, DOC, KEY, VALUE, session)
            result = await service.get(GUILD_ID, DOC, KEY, session)
            await session.abort_transaction()
    assert result == VALUE


async def test_empty_get(settings_client):
    result = None
    async with await settings_client.start_session() as session:
        async with session.start_transaction():
            await service.replace(GUILD_ID, DOC, KEY, VALUE, session)
            result = await service.get(GUILD_ID, DOC, "invalid_key", session)
            await session.abort_transaction()
    assert result is None


async def test_delete(settings_client):
    result = None
    async with await settings_client.start_session() as session:
        async with session.start_transaction():
            await service.replace(GUILD_ID, DOC, KEY, VALUE, session)
            result = await service.delete(GUILD_ID, DOC, KEY, session)
            await session.abort_transaction()
    assert result.deleted_count == 1


async def test_drop(settings_client):
    result = None
    async with await settings_client.start_session() as session:
        await service.replace(GUILD_ID, DOC, KEY, VALUE, session)
        await service.drop(GUILD_ID, DOC, session)
        result = await service.get(GUILD_ID, DOC, KEY, session)
    assert result is None


async def test_list(settings_client):
    result = None
    async with await settings_client.start_session() as session:
        async with session.start_transaction():
            await service.replace(GUILD_ID, DOC, KEY, VALUE, session)
            result = await service.list_keys(GUILD_ID, DOC, session)
            await session.abort_transaction()
    assert len(result) > 0
