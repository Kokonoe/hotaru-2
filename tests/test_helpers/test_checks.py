# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury

from types import SimpleNamespace
from typing import cast

from interactions import (
    CommandContext as sctx,
    Permissions as gperm,
    Snowflake,
)
from pytest import mark

from app.conf import owners
from app.settings import Setting, Settings
from app.settings.validators import IntValidator
from helpers.checks import (
    allowed_channel,
    has_command_permissions,
    has_guild_permissions,
    has_permissions,
    is_owner,
)
from helpers.permissions import CommandPermissions as cperm

pytestmark = mark.asyncio

AUTHOR_ID = Snowflake("1122334455667788")
GUILD_ID = Snowflake("0000000000000000")
CHANNEL_ID = Snowflake("1111111111111111")


class MockModule():

    def __init__(self, settings: Settings = Settings()):
        self.settings = settings


class MockContext():

    def __init__(self):
        self.author = SimpleNamespace(
            id=AUTHOR_ID,
            permissions=gperm.SEND_MESSAGES | gperm.ADD_REACTIONS,
        )
        self.channel_id = CHANNEL_ID
        self.data = SimpleNamespace(
            custom_id="Mock",
            name="Mock",
            options=None,
        )
        self.guild_id = GUILD_ID
        self.member = SimpleNamespace(id=AUTHOR_ID, roles=str(AUTHOR_ID))

    async def get_guild(self):
        return SimpleNamespace(roles=[SimpleNamespace(id=GUILD_ID, position=0)])


def test_is_owner_happy():
    cog = MockModule()
    ctx = MockContext()
    ctx.author.id = Snowflake(owners[0])
    result = is_owner().predicate(cog, ctx)
    assert result is True


def test_is_owner_sad():
    cog = MockModule()
    ctx = MockContext()
    result = is_owner().predicate(cog, ctx)
    assert result is False


async def test_has_guild_permissions_happy():
    cog = MockModule()
    ctx = MockContext()
    result = await has_guild_permissions(gperm.SEND_MESSAGES
                                        ).predicate(cog, ctx)
    assert result is True


async def test_has_guild_permissions_sad():
    cog = MockModule()
    ctx = MockContext()
    result = await has_guild_permissions(gperm.ADMINISTRATOR
                                        ).predicate(cog, ctx)
    assert result is False


async def test_has_guild_permissions_settings_happy():
    settings = Settings(
        Setting("MockModule", "guild_permissions", gperm.SEND_MESSAGES,
                IntValidator()))
    cog = MockModule(settings)
    ctx = MockContext()
    result = await has_guild_permissions().predicate(cog, ctx)
    assert result is True


async def test_has_guild_permissions_settings_sad():
    settings = Settings(
        Setting("MockModule", "guild_permissions", gperm.ADMINISTRATOR,
                IntValidator()))
    cog = MockModule(settings)
    ctx = MockContext()
    result = await has_guild_permissions().predicate(cog, ctx)
    assert result is False


async def test_has_command_permissions_happy(settings_client):
    cog = MockModule()
    ctx = cast(sctx, MockContext())
    result = None
    async with await settings_client.start_session() as session:
        async with session.start_transaction():
            permission: cperm = cperm(ctx, session=session)
            permission.perms.users_whitelist.add(str(AUTHOR_ID))
            await permission.set()
            result = await has_command_permissions(session=session
                                                  ).predicate(cog, ctx)
            await session.abort_transaction()
    assert result is True


async def test_has_command_permissions_sad():
    cog = MockModule()
    ctx = MockContext()
    result = await has_command_permissions().predicate(cog, ctx)
    assert result is False


async def test_has_permissions_command_happy(settings_client):
    cog = MockModule()
    ctx = cast(sctx, MockContext())
    result = None
    async with await settings_client.start_session() as session:
        async with session.start_transaction():
            permission: cperm = cperm(ctx, session=session)
            permission.perms.users_whitelist.add(str(AUTHOR_ID))
            await permission.set()
            result = await has_permissions(gperm.ADMINISTRATOR,
                                           session=session).predicate(cog, ctx)
            await session.abort_transaction()
    assert result is True


async def test_has_permissions_guild_happy():
    cog = MockModule()
    ctx = MockContext()
    result = await has_permissions(gperm.ADD_REACTIONS).predicate(cog, ctx)
    assert result is True


async def test_has_permissions_sad():
    cog = MockModule()
    ctx = MockContext()
    result = await has_permissions(gperm.ADMINISTRATOR).predicate(cog, ctx)
    assert result is False


async def test_allowed_channel_happy(settings_client):
    cog = MockModule()
    ctx = cast(sctx, MockContext())
    result = None
    async with await settings_client.start_session() as session:
        async with session.start_transaction():
            permission: cperm = cperm(ctx, session=session)
            permission.perms.channels_whitelist.add(str(CHANNEL_ID))
            await permission.set()
            result = await allowed_channel(session).predicate(cog, ctx)
            await session.abort_transaction()
    assert result is True


async def test_allowed_channel_sad():
    cog = MockModule()
    ctx = MockContext()
    result = await allowed_channel().predicate(cog, ctx)
    assert result is False


async def test_allowed_channel_everywhere(settings_client):
    cog = MockModule()
    ctx = cast(sctx, MockContext())
    result = None
    async with await settings_client.start_session() as session:
        async with session.start_transaction():
            permission: cperm = cperm(ctx, session=session)
            permission.perms.channels_allow_all = True
            await permission.set()
            result = await allowed_channel(session).predicate(cog, ctx)
            await session.abort_transaction()
    assert result is True
