# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury

from types import SimpleNamespace
from typing import cast

from interactions import (
    ActionRow,
    Button,
    ButtonStyle,
    CommandContext as sctx,
    Component,
    ComponentContext as cctx,
    InteractionData,
    Option,
    OptionType as opt,
)
from pytest import mark

from helpers.basics import conf, Embed, quip, say
from helpers.startup import get_config

pytestmark = mark.asyncio

MESSAGE: str = "Words go here."
QUIP: str = "Words go {location}."
SUBCOMMAND: str = "{things} go here."
COMPONENT: str = "Words {action} here."
conf.quips["mock"] = {"default": QUIP, "context": {"default": SUBCOMMAND}}
conf.quips["components"]["mock"] = {"default": COMPONENT}

config = get_config("hotaru")
buttons = {
    "red":
    Button(style=ButtonStyle.DANGER, label="A Red Button", custom_id="red"),
    "green":
    Button(style=ButtonStyle.SUCCESS,
           label="A Green Button",
           custom_id="green"),
    "blue":
    Button(style=ButtonStyle.PRIMARY, label="A Blue Button", custom_id="blue"),
}
button_row = [ActionRow.new(*buttons.values())]


class MockContext():

    def __init__(self):
        self.data = cast(InteractionData,
                         SimpleNamespace(
                             name="mock",
                             options=None,
                         ))

    @staticmethod
    async def send(embeds: Embed | None = None,
                   components: list[Component] | None = None,
                   ephemeral: bool = False):
        assert embeds
        assert embeds._json["description"] == MESSAGE
        if components:
            assert components[0].components
            for component in components[0].components:
                component: Component
                assert component.custom_id
                assert component.label == buttons[component.custom_id].label
        assert isinstance(ephemeral, bool)

    @staticmethod
    async def edit(embeds: Embed | None = None,
                   components: list[Component] | None = None):
        assert embeds
        assert embeds._json["description"] == MESSAGE
        if components:
            assert components[0].components
            for component in components[0].components:
                component: Component
                assert component.custom_id
                assert component.label == buttons[component.custom_id].label


class MockCommand(MockContext, sctx):
    pass


class MockComponent(MockContext, cctx):
    pass


def test_embed():
    embed: Embed = Embed(description=MESSAGE)
    assert embed._json["color"] == config["color"]
    assert embed._json["description"] == MESSAGE


async def test_say():
    ctx: sctx = MockCommand()
    await say(ctx, MESSAGE, components=ActionRow.new(*buttons.values()))


async def test_quip_command():
    ctx: sctx = MockCommand()
    await quip(ctx,
               components=ActionRow.new(*buttons.values()),
               hidden=True,
               location="here")


async def test_quip_subcommand():
    ctx: sctx = MockCommand()
    ctx.data.options = [
        Option(name="context", description=SUBCOMMAND, type=opt.SUB_COMMAND)
    ]
    await quip(ctx,
               components=ActionRow.new(*buttons.values()),
               hidden=True,
               things="Words")


async def test_quip_component():
    ctx: cctx = MockComponent()
    ctx.data.custom_id = "mock"
    await quip(ctx,
               components=ActionRow.new(*buttons.values()),
               hidden=True,
               action="go")
