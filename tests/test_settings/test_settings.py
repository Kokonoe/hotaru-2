# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury

import pytest

from app.settings import Setting, Settings
from app.settings import TypeValidator

SETTING_STRING = Setting("test_settings", "string", "", TypeValidator(str))
SETTING_BOOL = Setting("test_settings", "bool", True, TypeValidator(bool))
SETTING_INT = Setting("test_settings", "int", 0, TypeValidator(int))

GUILD_ID = "000000000000000000"
DOC = "user_dict"
KEY = "python"
VALUE = "snek"

pytestmark = pytest.mark.asyncio


async def test_settings_get(settings_client):
    found = 0
    async with await settings_client.start_session() as session:
        async with session.start_transaction():
            settings = Settings(SETTING_STRING, SETTING_BOOL)
            settings.set(SETTING_INT)
            if await settings["string"].get(session=session) == "":
                found += 1
            if await settings["bool"].get(session=session) is True:
                found += 1
            if await settings["int"].get(session=session) == 0:
                found += 1
            await session.abort_transaction()
    assert found == 3


async def test_settings_set(settings_client):
    result = None
    async with await settings_client.start_session() as session:
        async with session.start_transaction():
            setting = SETTING_STRING
            await setting.set(GUILD_ID, VALUE, session)
            result = await setting.get(GUILD_ID, session)
            await session.abort_transaction()
    assert result == VALUE


async def test_settings_delete(settings_client):
    result = None
    async with await settings_client.start_session() as session:
        async with session.start_transaction():
            setting = SETTING_STRING
            await setting.set(GUILD_ID, VALUE, session)
            result = await setting.delete(GUILD_ID, session)
            await session.abort_transaction()
    assert result


async def test_settings_invoke(settings_client):
    count = 0
    async with await settings_client.start_session() as session:
        async with session.start_transaction():
            settings = Settings(SETTING_INT)
            if await settings.int.get() == 0:
                count += 1
            if await settings.int() == 0:
                count += 1
            if await settings["int"].get() == 0:
                count += 1
            if await settings["int"]() == 0:
                count += 1
            if await settings.get("int").get() == 0:
                count += 1
            if await settings.get("int")() == 0:
                count += 1
            await session.abort_transaction()
    assert count == 6
