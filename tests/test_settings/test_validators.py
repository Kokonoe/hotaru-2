# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury

# pyright: reportGeneralTypeIssues=false

from pytest import fixture, raises

from app.settings import Validator, TypeValidator, IntValidator, BoolValidator


@fixture
def validate_int() -> IntValidator:
    return IntValidator(0, 10)


def test_interface():
    with raises(TypeError):
        Validator(type=str)


def test_type_string_happy():
    validate_str = TypeValidator(str)
    assert validate_str("string")


def test_type_string_sad():
    validate_str = TypeValidator(str)
    with raises(TypeError):
        validate_str(13)


def test_type_string_converted():
    validate_str = TypeValidator(str, convert=True)
    assert validate_str(12) == "12"


def test_int_true(validate_int):
    assert validate_int(5)


def test_int_wrong_type_float(validate_int):
    with raises(TypeError):
        validate_int(3.3)


def test_int_wrong_type_string(validate_int):
    with raises(TypeError):
        validate_int("potat")


def test_int_out_of_bounds(validate_int):
    with raises(ValueError):
        validate_int(15)


def test_bool_converted():
    validate_bool = BoolValidator(convert=True)
    assert validate_bool("True") is True


def test_bool_not_converted():
    validate_bool = BoolValidator()
    with raises(TypeError):
        validate_bool("True")
