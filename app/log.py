# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""Logging management for the app."""

from logging import basicConfig, Formatter, Handler, Logger
from logging.handlers import TimedRotatingFileHandler
from pathlib import Path
import logging


def configure_logging(name: str,
                      level: int = logging.INFO,
                      backup_days: int = 7) -> None:
    """Sets up a rotating log in the logs/ folder.

    Args:
        backup_days: Number of days to keep logs for before deleting them.
            Defaults to 7.
    """

    def namer(default_name: str) -> str:
        name, ext, date = default_name.split(".")
        return ".".join([name, date, ext])

    basicConfig(encoding="utf-8",
                level=logging.WARNING,
                style="{",
                format="{asctime}|{name}|{levelname}|{message}")
    Path("logs").mkdir(exist_ok=True)
    handler: Handler = TimedRotatingFileHandler("logs/log.log",
                                                "midnight",
                                                encoding="utf-8",
                                                backupCount=backup_days)
    formatter: Formatter = Formatter("{asctime}|{name}|{levelname}|{message}",
                                     style="{")
    handler.setFormatter(formatter)
    handler.namer = namer
    logger: Logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
