# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""The logs.service module interfaces between the app and the database."""

from typing import Any, Mapping

from pymongo.results import (
    DeleteResult as dres,
    InsertOneResult as ires,
    UpdateResult as ures,
)

from app.database import logs_service as service
from app.settings.validators import StrValidator

pad: StrValidator = StrValidator(16, True)
GLOBAL: str = pad.validate(0)


async def put_global(doc: str,
                     entry: Mapping[str, Any],
                     session=None) -> str | None:
    """Append a global log entry.

    Args:
        doc: The entry's document, usually the name of the module.
        entry: The setting's entry as a dictionary.
        session: A motor client session allowing transaction control.

    Returns:
        The set value.
    """
    resp: ires = await service.put_one(GLOBAL, doc, entry, session)
    if resp.acknowledged:
        return resp.inserted_id


async def replace_global(doc: str,
                         key: Mapping[str, Any],
                         entry: Mapping[str, Any],
                         session=None) -> bool:
    """Replace a global log entry.

    Args:
        doc: The entry's document, usually the name of the module.
        key: The key dictionary to identify the document with.
        entry: The entry document to insert.
        session: The AsyncIOMotorClientSession, used for transaction control.

    Returns:
        UpdateResult
    """
    resp: ures = await service.replace_one(GLOBAL, doc, key, entry, session)
    return resp.acknowledged


async def get_global(doc: str,
                     key: Mapping[str, Any],
                     session=None) -> list[dict[str, Any]]:
    """Retrieve a list of global log entries.

    Args:
        doc: The entry's document, usually the name of the module.
        key: The key dictionary to identify the document with.
        session: A motor client session allowing transaction control.

    Returns:
        A list of documents.
    """
    return await service.get_many(GLOBAL, doc, key, session)


async def delete_global(doc: str,
                        key: Mapping[str, Any],
                        session=None) -> bool:
    """Delete a global log entry.

    Args:
        doc: The entry's document, usually the name of the module.
        key: The key dictionary to identify the document with.
        session: A motor client session allowing transaction control.

    Returns:
        bool: The delete query's acknowledged status.
    """
    result: dres = await service.delete_one(GLOBAL, doc, key, session)
    return result.acknowledged


async def put(guild_id: str,
              doc: str,
              entry: Mapping[str, Any],
              session=None) -> str | None:
    """Append a log entry for the current guild.

    Args:
        guild_id: The guild_id for use as a collection name.
        doc: The entry's document, usually the name of the module.
        entry: The setting's entry as a dictionary.
        session: A motor client session allowing transaction control.

    Returns:
        The id of the database entry.
    """
    resp: ires = await service.put_one(guild_id, doc, entry, session)
    if resp.acknowledged:
        return resp.inserted_id


async def replace(guild_id: str,
                  doc: str,
                  key: Mapping[str, Any],
                  entry: Mapping[str, Any],
                  session=None) -> bool:
    """Replace a log entry for the current guild.

    Args:
        guild_id: The collection name, in this case the guild_id.
        doc: The entry's document, usually the name of the module.
        key: The key dictionary to identify the document with.
        entry: The entry document to insert.
        session: The AsyncIOMotorClientSession, used for transaction control.

    Returns:
        UpdateResult
    """
    resp: ures = await service.replace_one(guild_id, doc, key, entry, session)
    return resp.acknowledged


async def get(guild_id: str,
              doc: str,
              key: Mapping[str, Any],
              session=None) -> list[dict[str, Any]]:
    """Retrieve a list of log entries from the current guild.

    Args:
        guild_id: The guild_id for use as a collection name.
        doc: The entry's document, usually the name of the module.
        key: The key dictionary to identify the document with.
        session: A motor client session allowing transaction control.

    Returns:
        A list of documents.
    """
    return await service.get_many(guild_id, doc, key, session)


async def delete(guild_id: str,
                 doc: str,
                 key: Mapping[str, Any],
                 session=None) -> bool:
    """Delete a log entry from the current guild.

    Args:
        guild_id: The guild_id for use as a collection name.
        doc: The entry's document, usually the name of the module.
        key: The key dictionary to identify the document with.
        session: A motor client session allowing transaction control.

    Returns:
        bool: The delete query's acknowledged status.
    """
    result: dres = await service.delete_one(guild_id, doc, key, session)
    return result.acknowledged
