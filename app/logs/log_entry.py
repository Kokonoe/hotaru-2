# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury

from __future__ import annotations
from abc import abstractmethod
from dataclasses import dataclass, field
from datetime import datetime
from typing import Any, Type, TypeVar

from app.logs import service
from helpers.classes import AbstractDataClass

TLog = TypeVar("TLog", bound="LogEntry")
DOC = "EntryTypeName"


@dataclass
class LogEntry(AbstractDataClass):
    """The base LogEntry abstract class."""

    doc: str = field(default=DOC, kw_only=True)
    record_no: str | None
    timestamp: datetime
    note: str

    @abstractmethod
    def to_dict(self) -> dict[str, Any]:
        return NotImplemented

    @classmethod
    @abstractmethod
    def from_dict(cls: Type[TLog], **kwargs) -> TLog:
        return NotImplemented

    async def put(self, guild_id: str, session=None) -> bool:
        result = await service.put(guild_id, self.doc, self.to_dict(), session)
        return result is not None

    @classmethod
    async def get(cls: Type[TLog],
                  guild_id: str,
                  key: dict[str, Any],
                  session=None) -> list[TLog]:
        results = await service.get(guild_id, cls.doc, key, session)
        entries = [cls.from_dict(**result) for result in results]
        return entries

    @classmethod
    async def delete(cls,
                     guild_id: str,
                     key: dict[str, Any],
                     session=None) -> bool:
        result = await service.delete(guild_id, cls.doc, key, session)
        return result
