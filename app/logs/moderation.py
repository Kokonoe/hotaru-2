# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""The audit log module helps record moderation actions on users."""

from __future__ import annotations
from dataclasses import dataclass, field
from datetime import datetime
from enum import IntEnum
from typing import Any
from typing_extensions import NotRequired, TypedDict, Unpack

from app.logs import service
from app.logs.log_entry import LogEntry
from app.settings import StrValidator

pad: StrValidator = StrValidator(16, True)
DOC: str = "moderation"

TIME = "timestamp"
NOTE = "note"
ACTION = "action_type"
MOD = "moderator_id"
USER = "user_id"
ACTIVE = "active"
ORIGINAL = "original"
ID = "_id"


class ModerationActionType(IntEnum):
    WARN = 1
    TIMEOUT = 2
    KICK = 3
    BAN = 4
    UNWARN = 5
    UNTIMEOUT = 6
    UNBAN = 7
    PIN = 8
    UNPIN = 9
    DELETE = 10


class ModerationEntryDict(TypedDict):
    _id: NotRequired[str]
    timestamp: datetime
    note: str
    action_type: ModerationActionType
    moderator_id: str
    user_id: str
    active: bool | None
    original: str | None


@dataclass
class ModerationEntry(LogEntry):
    """The ModerationEntry object represents a single moderation log entry.

    The "active" argument is intended for the ModerationActionType WARN and
    TIMEOUT, and determines whether the action is still ongoing. The "original"
    argument is for UNWARN, UNTIMEOUT, and UNBAN, and refers to the original
    moderation action that is being reversed.
    """

    doc: str = field(default=DOC, kw_only=True)
    action_type: ModerationActionType
    moderator_id: str
    user_id: str
    active: bool | None = None
    original: str | None = None

    def to_dict(self) -> ModerationEntryDict:
        entry: ModerationEntryDict = {
            TIME: self.timestamp,
            NOTE: self.note,
            ACTION: self.action_type,
            MOD: self.moderator_id,
            USER: self.user_id,
            ACTIVE: self.active,
            ORIGINAL: self.original,
        }
        if self.record_no is not None:
            entry[ID] = self.record_no
        return entry

    @classmethod
    def from_dict(cls,
                  **kwargs: Unpack[ModerationEntryDict]) -> ModerationEntry:
        return ModerationEntry(kwargs.get(ID, None), kwargs[TIME], kwargs[NOTE],
                               ModerationActionType(kwargs[ACTION]),
                               kwargs[MOD], kwargs[USER], kwargs[ACTIVE],
                               kwargs[ORIGINAL])


async def put_moderation_entry(guild_id: str,
                               entry: ModerationEntry) -> str | None:
    return await service.put(guild_id, DOC, entry.to_dict())


async def update_moderation_entry(guild_id: str,
                                  entry: ModerationEntry) -> bool:
    return await service.replace(guild_id, DOC, {ID: entry.record_no},
                                 entry.to_dict())


async def get_moderation_entries_by_action_type(
        guild_id: str,
        action_type: ModerationActionType) -> list[ModerationEntry]:
    entries: list[dict[str, Any]] = await service.get(guild_id, DOC,
                                                      {ACTION: action_type})
    return [ModerationEntry.from_dict(**entry) for entry in entries]


async def get_moderation_entries_by_moderator(
        guild_id: str,
        moderator_id: str,
        action_type: ModerationActionType | None = None,
        active: bool | None = None) -> list[ModerationEntry]:
    key: dict[str, Any] = {MOD: moderator_id}
    if action_type:
        key[ACTION] = action_type
    if active:
        key[ACTIVE] = active
    entries: list[dict[str, Any]] = await service.get(guild_id, DOC, key)
    return [ModerationEntry.from_dict(**entry) for entry in entries]


async def get_moderation_entries_by_user(
        guild_id: str,
        user_id: str,
        action_type: ModerationActionType | None = None,
        active: bool | None = None) -> list[ModerationEntry]:
    key: dict[str, Any] = {USER: user_id}
    if action_type:
        key[ACTION] = action_type
    if active:
        key[ACTIVE] = active
    entries: list[dict[str, Any]] = await service.get(guild_id, DOC, key)
    return [ModerationEntry.from_dict(**entry) for entry in entries]


async def delete_moderation_entry(guild_id: str, record_no: str) -> bool:
    return await service.delete(guild_id, DOC, {ID: record_no})
