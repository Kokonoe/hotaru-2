# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""The economy log module records economy transactions."""

from __future__ import annotations
from dataclasses import dataclass, field
from datetime import datetime
from enum import IntEnum
from typing import Any
from typing_extensions import NotRequired, TypedDict, Unpack

from app.logs import service
from app.logs.log_entry import LogEntry
from app.settings import StrValidator

pad: StrValidator = StrValidator(16, True)
DOC: str = "economy"
GLOBAL: str = pad.validate(0)

TIME = "timestamp"
NOTE = "note"
ACTION = "action_type"
CURRENCY = "currency"
AMOUNT = "amount"
TOTAL = "total"
SOURCE = "source_id"
TARGET = "target_id"
ID = "_id"


class CurrencyType(IntEnum):
    """Currency codes for the economy module.

    You can just number your currencies locally, but colors are provided for
    compatibility with Hotaru 1.x

    ...and also because I like them.
    """
    RED = 1
    GREEN = 2
    BLUE = 3
    CYAN = 4
    MAGENTA = 5
    YELLOW = 6
    BLACK = 7
    WHITE = 8


class CurrencyActionType(IntEnum):
    """Transaction enums for the economy module.

    GET and SPEND refer to currency created or destroyed by the system.
    GIVE and RECIEVE refer to currency traded between users.
    SET and RESET refer to currency changed by moderator intervention,
        with SET meaning "currency value has been changed" and RESET meaning
        "currency value has been initialized to 0".
    """
    GET = 1
    SPEND = 2
    GIVE = 3
    RECIEVE = 4
    SET = 5
    RESET = 6


class EconomyEntryDict(TypedDict):
    _id: NotRequired[str]
    timestamp: datetime
    note: str
    action_type: CurrencyActionType
    currency: CurrencyType
    amount: int
    total: int
    source_id: str
    target_id: str | None


@dataclass
class EconomyEntry(LogEntry):
    """The EconomyTransaction object represents one exchange of currency.

    The "target_user_id" argument is for the GIVE and RECIEVE
    CurrencyActionType. "amount" refers to deltas, and "total" refers to the
    user's balance.
    """

    doc: str = field(default=DOC, kw_only=True)
    action_type: CurrencyActionType
    currency: CurrencyType
    amount: int
    total: int
    source_user_id: str
    target_user_id: str | None

    def to_dict(self) -> EconomyEntryDict:
        entry: EconomyEntryDict = {
            TIME: self.timestamp,
            NOTE: self.note,
            ACTION: self.action_type,
            CURRENCY: self.currency,
            AMOUNT: self.amount,
            TOTAL: self.total,
            SOURCE: self.source_user_id,
            TARGET: self.target_user_id,
        }
        if self.record_no is not None:
            entry[ID] = self.record_no
        return entry

    @classmethod
    def from_dict(cls, **kwargs: Unpack[EconomyEntryDict]) -> EconomyEntry:
        return EconomyEntry(kwargs.get(ID, None), kwargs[TIME], kwargs[NOTE],
                            CurrencyActionType(kwargs[ACTION]),
                            CurrencyType(kwargs[CURRENCY]), kwargs[AMOUNT],
                            kwargs[TOTAL], kwargs[SOURCE], kwargs[TARGET])


async def put_economy_entry(guild_id: str, entry: EconomyEntry) -> str | None:
    return await service.put(guild_id, DOC, entry.to_dict())


async def update_economy_entry(guild_id: str, entry: EconomyEntry) -> bool:
    return await service.replace(guild_id, DOC, {ID: entry.record_no},
                                 entry.to_dict())


async def get_economy_entries_by_action_type(
        guild_id: str, action_type: CurrencyActionType) -> list[EconomyEntry]:
    entries: list[dict[str, Any]] = await service.get(guild_id, DOC,
                                                      {ACTION: action_type})
    return [EconomyEntry.from_dict(**entry) for entry in entries]


async def get_economy_entries_by_user(
        guild_id: str,
        user_id: str,
        action_type: CurrencyActionType | None = None) -> list[EconomyEntry]:
    key_1: dict[str, Any] = {SOURCE: user_id}
    key_2: dict[str, Any] = {TARGET: user_id}
    if action_type:
        key_1[ACTION] = action_type
        key_2[ACTION] = action_type

    entries_1: list[dict[str, Any]] = await service.get(guild_id, DOC, key_1)
    entries_2: list[dict[str, Any]] = await service.get(guild_id, DOC, key_2)
    total_entries = entries_1 + entries_2
    total_entries.sort(key=lambda x: x[TIME])

    return [EconomyEntry.from_dict(**entry) for entry in total_entries]
