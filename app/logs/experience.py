# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""The experience log module records experience sources for users."""

from __future__ import annotations
from dataclasses import dataclass, field
from datetime import datetime
from enum import IntEnum
from typing import Any
from typing_extensions import NotRequired, TypedDict, Unpack

from app.logs import service
from app.logs.log_entry import LogEntry
from app.settings import StrValidator

pad: StrValidator = StrValidator(16, True)
DOC: str = "experience"

TIME = "timestamp"
NOTE = "note"
ACTION = "action_type"
MOD = "moderator_id"
USER = "user_id"
AMOUNT = "amount"
TOTAL = "total"
ID = "_id"


class ExperienceActionType(IntEnum):
    """Transaction enums for the experience module.

    CHAT for normal message experience, AWARD for experience given by the bot
    or moderators as a gift or award, SET when number is hard-set for some
    reason, and RESET for when experience data is reset for a user.
    """
    CHAT = 1
    AWARD = 2
    SET = 3
    RESET = 4


class ExperienceEntryDict(TypedDict):
    _id: NotRequired[str]
    timestamp: datetime
    note: str
    action_type: ExperienceActionType
    moderator_id: str | None
    user_id: str
    amount: int
    total: int


@dataclass
class ExperienceEntry(LogEntry):
    """The ExperienceEntry object represents an experience event.

    The "moderator_id" argument denotes AWARD actions granted by a moderator.
    Absence of this field for an AWARD action would denote a bot action.
    "amount" refers to deltas, and "total" refers to the user's balance.
    """

    doc: str = field(default=DOC, kw_only=True)
    action_type: ExperienceActionType
    moderator_id: str | None
    user_id: str
    amount: int
    total: int

    def to_dict(self) -> ExperienceEntryDict:
        entry: ExperienceEntryDict = {
            TIME: self.timestamp,
            NOTE: self.note,
            ACTION: self.action_type,
            MOD: self.moderator_id,
            USER: self.user_id,
            AMOUNT: self.amount,
            TOTAL: self.total,
        }
        if self.record_no is not None:
            entry[ID] = self.record_no
        return entry

    @classmethod
    def from_dict(cls,
                  **kwargs: Unpack[ExperienceEntryDict]) -> ExperienceEntry:
        return ExperienceEntry(kwargs.get(ID, None), kwargs[TIME], kwargs[NOTE],
                               ExperienceActionType(kwargs[ACTION]),
                               kwargs[MOD], kwargs[USER], kwargs[AMOUNT],
                               kwargs[TOTAL])


async def put_experience_entry(guild_id: str,
                               entry: ExperienceEntry) -> str | None:
    return await service.put(guild_id, DOC, entry.to_dict())


async def update_experience_entry(guild_id: str,
                                  entry: ExperienceEntry) -> bool:
    return await service.replace(guild_id, DOC, {ID: entry.record_no},
                                 entry.to_dict())


async def get_experience_entries_by_action_type(
        guild_id: str,
        action_type: ExperienceActionType) -> list[ExperienceEntry]:
    entries: list[dict[str, Any]] = await service.get(guild_id, DOC,
                                                      {ACTION: action_type})
    return [ExperienceEntry.from_dict(**entry) for entry in entries]


async def get_experience_entries_by_user(
        guild_id: str,
        user_id: str,
        action_type: ExperienceActionType | None = None
) -> list[ExperienceEntry]:
    key: dict[str, Any] = {USER: user_id}
    if action_type:
        key[ACTION] = action_type

    entries: list[dict[str, Any]] = await service.get(guild_id, DOC, key)
    return [ExperienceEntry.from_dict(**entry) for entry in entries]
