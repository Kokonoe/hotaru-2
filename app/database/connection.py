# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""The database connection manager for the bot.

The connection manager actually returns two clients corresponding to Hotaru's
two databases, one each for user data and settings data.

There shouldn't be any reason to call this module directly. The
settings_service and users_service modules handle CRUD operations for the two
databases.
"""
from asyncio import AbstractEventLoop

from motor.motor_asyncio import AsyncIOMotorClient

from helpers import startup


class Database():
    """The database connections manager class.

    Creates a connection to each of Hotaru's user and settings databases.

    Args:
        io_loop: The asyncio.AbstractEventLoop to use. It's usually not
            necessary to specify this, although it's useful for testing.
    """

    def __init__(self, io_loop: AbstractEventLoop | None = None) -> None:
        self.connect: dict = startup.get_config("mongo_connect")
        self.client = AsyncIOMotorClient(self.connect["host"],
                                         self.connect["port"])
        if io_loop is not None:
            self.client.io_loop = io_loop
        self.settings = self.connection("settings")
        self.users = self.connection("users")
        self.logs = self.connection("logs")

    def connection(self, category: str):
        """Create a mongo database connection.

        Args:
            category: Either "users" or "settings" to denote the database.

        Raises:
            ValueError: The input category was invalid.

        Returns:
            AsyncIOMotorDatabase: The database object.
        """
        if category not in ["settings", "users", "logs"]:
            raise ValueError("Not a valid database type.")
        db = self.client["_".join([self.connect["db"], category])]
        return db


db = Database()
