# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""The users service for database operations."""

from typing import Any

from pymongo.collection import ReturnDocument
from pymongo.results import (
    DeleteResult as dres,
    UpdateResult as ures,
)

from app.database.connection import db
from app.settings import StrValidator

pad: StrValidator = StrValidator(16, True)


async def replace(guild_id: str,
                  user_id: str,
                  key: str,
                  value: Any,
                  session = None) -> ures:
    """Replace an entry into the database.

    Technically an upsert, meaning the key will be created if it doesn't exist,
    and replaced if it does exist. In other words, this method doesn't create
    more than one instance of a specific key.

    Args:
        guild_id: The collection name, in this case the guild_id.
        user_id: The entry's document, in this case the user_id.
        key: The entry's key.
        value: The entry's value.
        session: The AsyncIOMotorClientSession, used for transaction control.

    Returns:
        UpdateResult
    """
    guild_id = pad.validate(guild_id)
    user_id = pad.validate(user_id)
    guild = db.users[guild_id]
    key = key.lower()
    entry = {"key": key, "value": value}
    result: ures = await guild[user_id].replace_one({"key": key},
                                                    entry,
                                                    upsert=True,
                                                    session=session)
    return result


async def increment(guild_id: str,
                    user_id: str,
                    key: str,
                    amount: int,
                    session = None) -> int:
    guild_id = pad.validate(guild_id)
    user_id = pad.validate(user_id)
    guild = db.users[guild_id]
    key = key.lower()
    entry = {"$inc": {"value": amount}}
    result: dict = await guild[user_id].find_one_and_update(
        {"key": key},
        entry,
        upsert=True,
        return_document=ReturnDocument.AFTER,
        session=session)
    return result["value"]


async def get(guild_id: str,
              user_id: str,
              key: str,
              session = None) -> Any:
    """Retrive a value from the database.

    Args:
        guild_id: The collection name, in this case the guild_id.
        user_id: The entry's document, in this case the user_id.
        key: The entry's key.
        session: The AsyncIOMotorClientSession, used for transaction control.

    Returns:
        The requested value.
    """
    guild_id = pad.validate(guild_id)
    user_id = pad.validate(user_id)
    guild = db.users[guild_id]
    req = {"key": key.lower()}
    result: dict | None = await guild[user_id].find_one(req, session=session)
    if result is not None:
        return result["value"]
    else:
        return None


async def delete(guild_id: str,
                 user_id: str,
                 key: str,
                 session = None) -> dres:
    """Delete a key from the database.

    Args:
        guild_id: The collection name, in this case the guild_id.
        user_id: The entry's document, in this case the user_id.
        key: The entry's key.
        session: The AsyncIOMotorClientSession, used for transaction control.

    Returns:
        DeleteResult
    """
    guild_id = pad.validate(guild_id)
    user_id = pad.validate(user_id)
    guild = db.users[guild_id]
    req = {"key": key.lower()}
    result: dres = await guild[user_id].delete_one(req, session=session)
    return result


async def drop(guild_id: str, user_id: str, session = None) -> None:
    """Drop a document from the database.

    Annoyingly doesn't return anything, so the operation can't be verified.

    Args:
        guild_id: The collection name, in this case the guild_id.
        user_id: The entry's document, in this case the user_id.
        session: The AsyncIOMotorClientSession, used for transaction control.
    """
    guild_id = pad.validate(guild_id)
    user_id = pad.validate(user_id)
    guild = db.users[guild_id]
    await guild[user_id].drop(session=session)


async def list_keys(guild_id: str,
                    user_id: str,
                    session = None) -> dict:
    """List the keys in the document.

    Args:
        guild_id: The collection name, in this case the guild_id.
        user_id: The entry's document, in this case the user_id.
        session: The AsyncIOMotorClientSession, used for transaction control.

    Returns:
        dict: A dictionary of key:value pairs in the document.
    """
    guild_id = pad.validate(guild_id)
    user_id = pad.validate(user_id)
    guild = db.users[guild_id]
    results: list[dict] = await guild[user_id].find(session=session
                                                   ).to_list(None)
    return {result["key"]: result["value"] for result in results}
