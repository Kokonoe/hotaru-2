# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""The logs service for database operations."""

from typing import Any, Mapping

from pymongo.results import (
    DeleteResult as dres,
    InsertOneResult as ires,
    UpdateResult as ures,
)

from app.database.connection import db
from app.settings import StrValidator

pad: StrValidator = StrValidator(16, True)


async def put_one(guild_id: str,
                  doc: str,
                  entry: Mapping[str, Any],
                  session=None) -> ires:
    """Insert an entry into the databse.

    Unlike replace, is not an upsert operation, meaning multiple entries with
    the same key can be created.

    Args:
        guild_id: The collection name, in this case the guild_id.
        doc: The entry's document, usually the name of the module.
        entry: The entry document to insert.
        session: The AsyncIOMotorClientSession, used for transaction control.

    Returns:
        InsertOneResult
    """
    guild_id = pad.validate(guild_id)
    guild = db.logs[guild_id]
    result: ires = await guild[doc].insert_one(entry, session=session)
    return result


async def replace_one(guild_id: str,
                      doc: str,
                      key: Mapping[str, Any],
                      entry: Mapping[str, Any],
                      session=None) -> ures:
    """Replace an entry into the database.

    Technically an upsert, meaning the key will be created if it doesn't exist,
    and replaced if it does exist. In other words, this method doesn't create
    more than one instance of a specific key. Not aware of duplicate keys, so
    should not be used if multiple instances of a key are expected.

    Args:
        guild_id: The collection name, in this case the guild_id.
        doc: The entry's document, usually the name of the module.
        key: The key dictionary to identify the document with.
        entry: The entry document to insert.
        session: The AsyncIOMotorClientSession, used for transaction control.

    Returns:
        UpdateResult
    """
    guild_id = pad.validate(guild_id)
    guild = db.logs[guild_id]
    result: ures = await guild[doc].replace_one(key,
                                                entry,
                                                upsert=True,
                                                session=session)
    return result


async def get_many(guild_id: str,
                   doc: str,
                   key: Mapping[str, Any],
                   session=None) -> list[dict[str, Any]]:
    """Retrieve a list of matching values from the database.

    Args:
        guild_id: The collection name, in this case the guild_id.
        doc: The entry's document, usually the name of the module.
        key: The key dictionary to identify the documents with.
        session: The AsyncIOMotorClientSession, used for transaction control.

    Returns:
        A list of documents.
    """
    guild_id = pad.validate(guild_id)
    guild = db.logs[guild_id]
    result = guild[doc].find(key, session=session)
    return await result.to_list(length=None)


async def delete_one(guild_id: str,
                     doc: str,
                     key: Mapping[str, Any],
                     session=None) -> dres:
    """Delete a key from the database.

    Args:
        guild_id: The collection name, in this case the guild_id.
        doc: The entry's document, usually the name of the module.
        key: The key dictionary to identify the document with.
        session: The AsyncIOMotorClientSession, used for transaction control.

    Returns:
        DeleteResult
    """
    guild_id = pad.validate(guild_id)
    guild = db.logs[guild_id]
    result: dres = await guild[doc].delete_one(key, session=session)
    return result
