# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""The configurations module. Holds shared settings for the app."""

from typing import Any

from helpers import startup

config: dict[str, Any] = startup.get_config("hotaru")
quips: dict[str, Any] = startup.get_quips()

owners: list[str] = [str(owner) for owner in config["owners"]]
scope: list[int] = [int(guild) for guild in config["scope"]]
settings: dict[str, Any] = {}

extensions: list[str] = startup.get_extensions(config["optional_cogs"],
                                               config["excluded_cogs"],
                                               config["external_cogs"])
