# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""The economy module manages guild currencies and trade."""

from app.logs.economy import CurrencyType as ctype
from app.users import service

client = service.service.db.client


async def check_balance(guild_id: str,
                        user_id: str,
                        currency: ctype,
                        amount: int,
                        session=None) -> bool:
    """Check if a user has at least [amount] of [currency_name].

    Useful before GIVE or SPEND actions.

    Args:
        guild_id (str)
        user_id (str)
        currency (CurrencyType): Type of currency to check
        amount (int): Amount of currency to check balance for
        session (ses, optional)

    Returns:
        bool: True if user has at least [amount] of [currency_name], else False
    """
    balance: int = await service.get(guild_id,
                                     user_id,
                                     currency.name,
                                     session=session)
    return True if balance >= amount else False


async def put(guild_id: str,
              user_id: str,
              currency: ctype,
              amount: int,
              session=None) -> int:
    return await service.set(guild_id,
                             user_id,
                             currency.name,
                             amount,
                             session=session)


async def increment(guild_id: str,
                    user_id: str,
                    currency: ctype,
                    amount_inc: int,
                    session=None) -> int:
    return await service.increment(guild_id,
                                   user_id,
                                   currency.name,
                                   amount_inc,
                                   session=session)


async def give(guild_id: str,
               user_id: str,
               recipient_id: str,
               currency: ctype,
               amount: int,
               session=None) -> bool:
    has_funds = await check_balance(guild_id,
                                    user_id,
                                    currency,
                                    amount,
                                    session=session)
    if has_funds:
        await increment(guild_id, user_id, currency, -amount, session=session)
        await increment(guild_id,
                        recipient_id,
                        currency,
                        amount,
                        session=session)
        return True
    else:
        return False
