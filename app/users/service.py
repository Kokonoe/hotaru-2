# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""The users.service module interfaces between the app and the database."""

from typing import Any

from pymongo.results import (
    DeleteResult as dres,
    UpdateResult as ures,
)

from app.database import users_service as service
from app.settings.validators import StrValidator, Validator

pad: StrValidator = StrValidator(16, True)
GLOBAL: str = pad.validate(0)


async def set_global(user_id: str,
                     key: str,
                     value: Any = None,
                     validate: Validator | None = None,
                     session=None) -> Any:
    """Set a global user value.

    Args:
        user_id: The user_id identifying the document.
        key: The setting's key.
        value: The setting's value.
        validate: Supplying a validator allows for data validation and type-
            casting between the app and database.
        session: A motor client session allowing transaction control.

    Returns:
        The set value.
    """
    if validate is not None:
        value = validate(value)
    resp: ures = await service.replace(GLOBAL, user_id, key, value, session)
    if resp.acknowledged:
        return value


async def increment_global(user_id: str,
                           key: str,
                           amount: int,
                           session=None) -> int:
    """Increments a global user value.

    Args:
        user_id: The user_id identifying the document.
        key: The setting's key.
        amount: The amount to increment.
        session: A motor client session allowing transaction control.

    Returns:
        The set value.
    """
    return await service.increment(GLOBAL, user_id, key, amount, session)


async def get_global(user_id: str,
                     key: str,
                     validate: Validator | None = None,
                     session=None) -> Any:
    """Get a global user value.

    Args:
        user_id: The user_id identifying the document.
        key: The setting's key.
        validate: Supplying a validator allows for data validation and type-
            casting between the app and database.
        session: A motor client session allowing transaction control.

    Returns:
        The requested value.
    """
    value = await service.get(GLOBAL, user_id, key, session)
    if validate is not None:
        value = validate(value)
    return value


async def delete_global(user_id: str, key: str, session=None) -> bool:
    """Delete a global user value.

    Args:
        user_id: The user_id identifying the document.
        key: The setting's key.
        session: A motor client session allowing transaction control.

    Returns:
        bool: The delete query's acknowledged status.
    """
    result: dres = await service.delete(GLOBAL, user_id, key, session)
    return result.acknowledged


async def drop_global(user_id: str, session=None) -> None:
    """Drops an entire user from the global collection.

    Args:
        user_id: The user_id identifying the document.
        key: The setting's key.
        value: The setting's value.
        validate: Supplying a validator allows for data validation and type-
            casting between the app and database.
        session: A motor client session allowing transaction control.
    """
    await service.drop(GLOBAL, user_id, session)


async def set(guild_id: str,
              user_id: str,
              key: str,
              value: Any,
              validate: Validator | None = None,
              session=None) -> Any:
    """Set a guild user value.

    Args:
        guild_id: The guild_id for use as a collection name.
        user_id: The user_id identifying the document.
        key: The setting's key.
        value: The setting's value.
        validate: Supplying a validator allows for data validation and type-
            casting between the app and database.
        session: A motor client session allowing transaction control.

    Returns:
        The set value.
    """
    if validate is not None:
        value = validate(value)
    resp: ures = await service.replace(guild_id, user_id, key, value, session)
    if resp.acknowledged:
        return value


async def increment(guild_id: str,
                    user_id: str,
                    key: str,
                    amount: int,
                    session=None) -> int:
    """Increments a guild user value.

    Args:
        user_id: The user_id identifying the document.
        key: The setting's key.
        amount: The amount to increment.
        session: A motor client session allowing transaction control.

    Returns:
        The set value.
    """
    return await service.increment(guild_id, user_id, key, amount, session)


async def get(guild_id: str,
              user_id: str,
              key: str,
              validate: Validator | None = None,
              session=None) -> Any:
    """Get a guild user value.

    If the setting doesn't exist, return the default value, instead.

    Args:
        guild_id: The guild_id for use as a collection name.
        user_id: The user_id identifying the document.
        key: The setting's key.
        validate: Supplying a validator allows for data validation and type-
            casting between the app and database.
        session: A motor client session allowing transaction control.

    Returns:
        The requested guild setting, or the default.
    """
    value = await service.get(guild_id, user_id, key, session)
    # * users.service will not return the default like settings.service will!
    if validate is not None:
        value = validate(value)
    return value


async def delete(guild_id: str, user_id: str, key: str, session=None) -> bool:
    """Delete a guild user value.

    Args:
        guild_id: The guild_id for use as a collection name.
        user_id: The user_id identifying the document.
        key: The setting's key.
        session: A motor client session allowing transaction control.

    Returns:
        bool: The delete query's acknowledged status.
    """
    result: dres = await service.delete(guild_id, user_id, key, session)
    return result.acknowledged


async def drop(guild_id: str, user_id: str, session=None) -> None:
    """Drop a whole user from the current guild.

    Args:
        guild_id: The guild_id for use as a collection name.
        user_id: The user_id identifying the document.
        session: A motor client session allowing transaction control.
    """
    await service.drop(guild_id, user_id, session)
