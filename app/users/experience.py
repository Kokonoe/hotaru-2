# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""The experience module manages user experience and levels."""

from datetime import datetime, timedelta
import math

from app.users import service

XP = "experience"
HISTORY = "history"
ROLES = "roles"
client = service.service.db.client


def calculate_level(multiplier: int, growth_ratio: float, xp: int) -> int:
    return math.floor((xp / (multiplier * growth_ratio))**(1. / 3.))


async def put(guild_id: str, user_id: str, xp: int, session=None) -> int:
    return await service.set(guild_id, user_id, XP, xp, session=session)


async def increment(guild_id: str,
                    user_id: str,
                    xp_inc: int,
                    session=None) -> int:
    return await service.increment(guild_id,
                                   user_id,
                                   XP,
                                   xp_inc,
                                   session=session)


async def get(guild_id: str, user_id: str, session=None) -> int:
    return await service.get(guild_id, user_id, XP, session=session)


async def get_history(guild_id: str,
                      user_id: str,
                      session=None) -> list[datetime]:
    response = await service.get(guild_id, user_id, HISTORY, session=session)
    return response if response else []


async def put_history(guild_id: str,
                      user_id: str,
                      timestamps: list[datetime],
                      session=None) -> list[datetime]:
    return await service.set(guild_id,
                             user_id,
                             HISTORY,
                             timestamps,
                             session=session)


async def get_roles(guild_id: str, user_id: str, session=None) -> set[int]:
    response = await service.get(guild_id, user_id, ROLES, session=session)
    return set(response) if response else set()


async def add_roles(guild_id: str,
                    user_id: str,
                    *role_ids: int,
                    session=None) -> set[int]:
    roles: set[int] = await get_roles(guild_id, user_id, session=session)
    for role_id in role_ids:
        roles.add(role_id)
    return await service.set(guild_id,
                             user_id,
                             ROLES,
                             list(roles),
                             session=session)


async def log_timestamp(guild_id: str,
                        user_id: str,
                        interval_seconds: int,
                        timestamp: datetime,
                        session=None) -> tuple[int, datetime | None]:
    """Logs message timestamps.

    Returns number of messages logged within previous interval, not including
    the current message.

    Args:
        guild_id (str)
        user_id (str)
        interval_seconds (int): Interval of time to keep timestamps. Older
                                timestamps are pruned.
        timestamp (datetime): Timestamp from the posted message

    Returns:
        tuple[int, datetime | None]: A tuple consisting of:
            int: Number of messages logged within the previous interval
            datetime | None: The most recent timestamp, if one exists
    """
    recent_entries: list[datetime] = []
    entries: list[datetime] = await get_history(guild_id,
                                                user_id,
                                                session=session)
    for entry in entries:
        if (datetime.utcnow() - entry) < timedelta(seconds=interval_seconds):
            recent_entries.append(entry)
    recent_entries.append(timestamp)
    await put_history(guild_id, user_id, recent_entries, session=session)
    return (len(recent_entries) - 1, entries[-1] if entries else None)
