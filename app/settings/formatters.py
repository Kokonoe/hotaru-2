# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""Formatters format an entry for display.

They should be thought of as the inverse to the validators: whereas a
validator prepares a user-supplied input for storage in the database, a
formatter prepares a retrieved database entry for display to the user.
"""

from abc import ABC, abstractmethod


class Formatter(ABC):
    """The abstract Formatter class."""

    @abstractmethod
    def __init__(self, **kwargs):
        self.pretty = True

    @abstractmethod
    def format(self, value) -> str:
        return str(value)

    def __call__(self, *args) -> str:
        return self.format(*args)


class StringFormatter(Formatter):
    """Casts incoming data as a String."""

    def __init__(self):
        pass

    def format(self, value) -> str:
        return str(value)
