# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""The settings.service module interfaces between the app and the database."""

from typing import Any

from pymongo.results import (
    DeleteResult as dres,
    UpdateResult as ures,
)

from app.database import settings_service as service
from app.settings.validators import StrValidator, Validator

pad: StrValidator = StrValidator(16, True)
GUILD_DEFAULT: str = pad.validate(0)


async def set_default(document: str,
                      key: str,
                      value: Any = None,
                      validate: Validator | None = None,
                      session=None) -> Any:
    """Set the global default setting.

    Args:
        document: The document to save the setting under. Usually corresponds to
            a cog for consistency, but can be anything.
        key: The setting's key.
        value: The setting's value.
        validate: Supplying a validator allows for data validation and type-
            casting between the app and database.
        session: A motor client session allowing transaction control.

    Returns:
        The set value.
    """
    if validate is not None:
        value = validate(value)
    resp: ures = await service.replace(GUILD_DEFAULT, document, key, value,
                                       session)
    if resp.acknowledged:
        return value


async def get_default(document: str,
                      key: str,
                      validate: Validator | None = None,
                      session=None) -> Any:
    """Get the global default setting.

    Args:
        document: The document to save the setting under. Usually corresponds to
            a cog for consistency, but can be anything.
        key: The setting's key.
        validate: Supplying a validator allows for data validation and type-
            casting between the app and database.
        session: A motor client session allowing transaction control.

    Returns:
        The requested value.
    """
    value = await service.get(GUILD_DEFAULT, document, key, session)
    if validate is not None:
        value = validate(value)
    return value


async def delete_default(document: str, key: str, session=None) -> bool:
    """Delete the global default setting.

    Args:
        document: The document to save the setting under. Usually corresponds to
            a cog for consistency, but can be anything.
        key: The setting's key.
        session: A motor client session allowing transaction control.

    Returns:
        bool: The delete query's acknowledged status.
    """
    result: dres = await service.delete(GUILD_DEFAULT, document, key, session)
    return result.acknowledged


async def drop_default(document: str, session=None) -> None:
    """Drops an entire document from the default collection.

    Args:
        document: The document to save the setting under. Usually corresponds to
            a cog for consistency, but can be anything.
        key: The setting's key.
        value: The setting's value.
        validate: Supplying a validator allows for data validation and type-
            casting between the app and database.
        session: A motor client session allowing transaction control.
    """
    await service.drop(GUILD_DEFAULT, document, session)


async def set(guild_id: str,
              document: str,
              key: str,
              value: Any,
              validate: Validator | None = None,
              session=None) -> Any:
    """Set a guild setting.

    Args:
        guild_id: The guild_id for use as a collection name.
        document: The document to save the setting under. Usually corresponds to
            a cog for consistency, but can be anything.
        key: The setting's key.
        value: The setting's value.
        validate: Supplying a validator allows for data validation and type-
            casting between the app and database.
        session: A motor client session allowing transaction control.

    Returns:
        The set value.
    """
    if validate is not None:
        value = validate(value)
    resp: ures = await service.replace(guild_id, document, key, value, session)
    if resp.acknowledged:
        return value


async def get(guild_id: str,
              document: str,
              key: str,
              validate: Validator | None = None,
              session=None) -> Any:
    """Get a setting for the current guild.

    If the setting doesn't exist, return the default value, instead.

    Args:
        guild_id: The guild_id for use as a collection name.
        document: The document to save the setting under. Usually corresponds to
            a cog for consistency, but can be anything.
        key: The setting's key.
        validate: Supplying a validator allows for data validation and type-
            casting between the app and database.
        session: A motor client session allowing transaction control.

    Returns:
        The requested guild setting, or the default.
    """
    value = await service.get(guild_id, document, key, session)
    if value is None:
        value = await get_default(document, key, session=session)
    if validate is not None:
        value = validate(value)
    return value


async def delete(guild_id: str, document: str, key: str, session=None) -> bool:
    """Delete a guild setting.

    Args:
        guild_id: The guild_id for use as a collection name.
        document: The document to save the setting under. Usually corresponds to
            a cog for consistency, but can be anything.
        key: The setting's key.
        session: A motor client session allowing transaction control.

    Returns:
        bool: The delete query's acknowledged status.
    """
    result: dres = await service.delete(guild_id, document, key, session)
    return result.acknowledged


async def drop(guild_id: str, document: str, session=None) -> None:
    """Drop a settings document from the current guild.

    Args:
        guild_id: The guild_id for use as a collection name.
        document: The document to save the setting under. Usually corresponds to
            a cog for consistency, but can be anything.
        session: A motor client session allowing transaction control.
    """
    await service.drop(guild_id, document, session)
