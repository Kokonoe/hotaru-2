# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""Setting objects to handle database operations."""

from __future__ import annotations
from typing import Any, Iterator

from interactions import Snowflake

from app.settings import service
from app.settings.formatters import Formatter
from app.settings.validators import Validator


class Setting():
    """The Setting class manages a single setting's database entries.

    A group of Setting objects is stored in the Settings container object,
    which can be set as an attribute in a cog to register the setting to the
    Setting cog.

    class MyCog(Extension):

        def __init__(self, bot: Client) -> None:
            self.bot: Client = bot
            self.settings: Settings = Settings(
                Setting("module_name", "setting_1", default_value),
                Setting("module_name", "setting_2", default_value)
            )

    Args:
        document: The setting group. This normally corresponds to the cog or
            module name, but if you need more groups for organizational
            purposes, feel free to name the documents whatever you like.
        key: The setting key.
        default: The default setting to fall back on if the guild setting is
            unset.
        validate: A validator ensures the type of set and get values matches a
            predetermined type, and can be configured to cast types, as well.
            This used to be very useful, for example, for bool values, which
            would be typed into the command as a str. It's less necessary now
            that Interactions handles basic type-checking, but it still has
            some specific use cases.
        format: A formatter converts between the input / output format and the
            database format.
    """

    def __init__(self,
                 document: str,
                 key: str,
                 default: Any = None,
                 validate: Validator | None = None,
                 format: Formatter | None = None) -> None:
        self.document = document
        self.key = key
        self.default = default
        self.validate = validate
        self.format = format

    async def set_default(self, value: Any, session=None) -> Any:
        """Set a global default setting in the database.

        If set, this value will override the one in the self.default attribute.

        Args:
            value: The setting's value.
            session: A motor client session enables transaction control.

        Returns:
            The set value.
        """
        value = await service.set_default(self.document, self.key, value,
                                          self.validate, session)
        if self.format:
            value = self.format.format(value)
        return value

    async def get_default(self, session=None) -> Any:
        """Get the global default setting from the database.

        Args:
            session: A motor client session enables transaction control.

        Returns:
            The requested value.
        """
        value = await service.get_default(self.document, self.key,
                                          self.validate, session)
        if self.format:
            value = self.format.format(value)
        return value

    async def delete_default(self, session=None) -> bool:
        """Delete the global default setting from the database.

        Args:
            session: A motor client session enables transaction control.

        Returns:
            bool: The acknowledgement status of the deletion.
        """
        return await service.delete_default(self.document, self.key, session)

    async def set(self,
                  guild_id: Snowflake | str,
                  value: Any,
                  session=None) -> Any:
        """Set a guild setting to the database.

        Args:
            guild_id: The guild_id to use as a collection name.
            value: The setting's value.
            session: A motor client session enables transaction control.

        Returns:
            The set value.
        """
        value = await service.set(str(guild_id), self.document, self.key,
                                  value, self.validate, session)
        if self.format:
            value = self.format.format(value)
        return value

    async def get(self,
                  guild_id: Snowflake | str | None = None,
                  session=None) -> Any:
        """Get a guild setting from the database.

        Args:
            guild_id: The guild_id to use as a collection name.
            session: A motor client session enables transaction control.

        Returns:
            The requested value.
        """
        value = None
        if guild_id is not None:
            try:
                value = await service.get(str(guild_id), self.document,
                                          self.key, self.validate, session)
            except TypeError:
                value = None
        if value is None:
            value = self.default
        if self.format:
            value = self.format.format(value)
        return value

    async def delete(self, guild_id: Snowflake | str, session=None) -> bool:
        """Delete a guild setting from the database.

        Args:
            guild_id: The guild_id to use as a collection name.
            session: A motor client session enables transaction control.

        Returns:
            bool: The acknowledgement status of the deletion.
        """
        return await service.delete(str(guild_id), self.document, self.key,
                                    session)

    async def __call__(self, guild_id: str | None = None, session=None) -> Any:
        return await self.get(guild_id, session)


class Settings():
    """The Settings container class houses Setting objects.

    Setting objects within can be accessed and modified using dictionary or
    attribute syntax, and can also can be iterated through.

    Args:
        *settings: A list of Setting objects.
    """

    def __init__(self, *settings: Setting) -> None:
        object.__setattr__(self, "settings", {})
        [self.set(setting) for setting in settings]

    def __iter__(self) -> Iterator[Setting]:
        for setting in self.settings.values():
            yield setting

    def __contains__(self, key) -> bool:
        self.settings: dict
        return key in self.settings

    def __setitem__(self, key: str, setting: Setting) -> None:
        if isinstance(setting, Setting):
            self.settings[key] = setting
        else:
            raise TypeError

    def __getitem__(self, key: str) -> Any:
        return self.get(key)

    def __delitem__(self, key: str) -> None:
        self.delete(key)

    def __setattr__(self, key: str, setting: Any) -> None:
        self.__setitem__(key, setting)

    def __getattr__(self, key: str) -> Any:
        return self.get(key)

    def __delattr__(self, key: str) -> None:
        self.delete(key)

    def set(self, setting: Setting) -> None:
        if isinstance(setting, Setting):
            self.settings[setting.key] = setting
        else:
            raise TypeError

    def get(self, key: str, default: Any = None) -> Any:
        if key in self.settings:
            return self.settings[key]
        else:
            return default

    def delete(self, key: str) -> None:
        if key in self.settings:
            del self.settings[key]
        else:
            raise KeyError
