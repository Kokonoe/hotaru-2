# Hotaru is an open-source and modular Discord moderation bot.
# Copyright (C) 2022 Kokonoe Mercury
"""Validators check user-supplied data for correct formatting."""

from abc import ABC, abstractmethod
from distutils.util import strtobool
from typing import Any


class Validator(ABC):
    """The abstract Validator class.

    Custom validator classes should override the validate and cast methods as
    needed, without changing the input args.

    Args:
        type: The Type class to use if converting.
        convert: Whether to cast data if the type mismatches.
    """

    @abstractmethod
    def __init__(self, *, convert: bool = False, **kwargs):
        self.type = kwargs["type"]
        self.convert = convert

    @abstractmethod
    def validate(self, value: Any) -> Any:
        """Validation logic.

        Args:
            value: The value to validate.

        Raises:
            TypeError: The error this raises should be changed to suit the
                logic of the validator, such as to a ValueError.

        Returns:
            The value, converted if convert is True.
        """
        if not isinstance(value, self.type):
            if self.convert:
                value = self.cast(value)
            else:
                raise TypeError
        return value

    @abstractmethod
    def cast(self, value: Any) -> Any:
        """Conversion logic.


        Args:
            value: The value to convert.

        Returns:
            The converted value.
        """
        return self.type(value)

    def __call__(self, *args) -> bool:
        return self.validate(*args)


class TypeValidator(Validator):
    """TypeValidator checks an input for a valid type.

    Args:
        type: The Type class to use if converting.
        convert: Whether to cast data if the type mismatches.
    """

    def __init__(self, type: type, *, convert: bool = False):
        self.type = type
        self.convert = convert

    def validate(self, value: Any) -> Any:
        if not isinstance(value, self.type):
            if self.convert:
                value = self.cast(value)
            else:
                raise TypeError
        return value

    def cast(self, value: Any) -> Any:
        return self.type(value)


class StrValidator(Validator):
    """StrValidator checks if an input is a string.

    Optionally, values can be padded.

    Args:
        zfill: Length to pad with zeroes, or None.
        convert: Whether to cast data if the type mismatches.
    """

    def __init__(self, zfill: int | None = None, convert: bool = False):
        self.zfill: int | None = zfill
        self.convert = convert

    def validate(self, value: Any) -> str:
        if not isinstance(value, str):
            if self.convert:
                value = self.cast(value)
            else:
                raise TypeError
        if self.zfill is not None:
            value = value.zfill(self.zfill)
        return value

    def cast(self, value: Any) -> str:
        return str(value)


class IntValidator(Validator):
    """IntValidator checks if an input is an integer.

    Optionally, a min and max value can be set.

    Args:
        min: The minimum allowed value.
        max: The maximum allowed value.
        convert: Whether to cast data if the type mismatches.
    """

    def __init__(self,
                 min: int | None = None,
                 max: int | None = None,
                 *,
                 convert: bool = False):
        self.min = min
        self.max = max
        self.convert = convert

    def validate(self, value: Any) -> int:
        """Validate the integer.

        Args:
            value: The value to validate.

        Raises:
            TypeError: The value cannot be cast to an int, or is not an int and
                convert is false.
            ValueError: The value exceeds the set min/max boundary.

        Returns:
            int: The value, converted if necessary.
        """
        if not isinstance(value, int):
            if self.convert is True:
                value = self.cast(value)
            else:
                raise TypeError(f"Value {value} not of type int")
        if self.min is not None and value < self.min:
            raise ValueError(f"{value} below minimum {self.min}")
        if self.max is not None and value > self.max:
            raise ValueError(f"{value} above maximum {self.max}")
        return value

    def cast(self, value: Any) -> int:
        return int(value)


class FloatValidator(Validator):
    """FloatValidator checks if an input is a float.

    Optionally, a min and max value can be set.

    Args:
        min: The minimum allowed value.
        max: The maximum allowed value.
        convert: Whether to cast data if the type mismatches.
    """

    def __init__(self,
                 min: float | None = None,
                 max: float | None = None,
                 *,
                 convert: bool = False):
        self.min = min
        self.max = max
        self.convert = convert

    def validate(self, value: Any) -> float:
        """Validate the float.

        Args:
            value: The value to validate.

        Raises:
            TypeError: The value cannot be cast to a float, or is not a float and
                convert is false.
            ValueError: The value exceeds the set min/max boundary.

        Returns:
            float: The value, converted if necessary.
        """
        if not isinstance(value, float):
            if self.convert is True:
                value = self.cast(value)
            else:
                raise TypeError(f"Value {value} not of type float")
        if self.min is not None and value < self.min:
            raise ValueError(f"{value} below minimum {self.min}")
        if self.max is not None and value > self.max:
            raise ValueError(f"{value} above maximum {self.max}")
        return value

    def cast(self, value: Any) -> float:
        return float(value)


class BoolValidator(Validator):
    """BoolValidator checks if an input is a bool.

    Useful to cast str representations of bool.

    Args:
        min: The minimum allowed value.
        max: The maximum allowed value.
        convert: Whether to cast data if the type mismatches.
    """

    def __init__(self, *, convert: bool = False):
        self.convert = convert

    def validate(self, value: Any) -> bool:
        """Validate the bool.

        Args:
            value: The value to validate.

        Raises:
            TypeError: The value cannot be cast to a bool, or is not a bool and
                convert is false.

        Returns:
            bool: The value, converted if necessary.
        """
        if not isinstance(value, bool):
            if self.convert is True:
                value = self.cast(value)
            else:
                raise TypeError(f"Value {value} not of type bool")
        return value

    def cast(self, value: Any) -> bool:
        if isinstance(value, str):
            value = strtobool(value)
        return bool(value)
